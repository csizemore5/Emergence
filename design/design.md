# Emergence Design Concept
## Setting
### Player
The player plays as a newly-sentient Artificial Intelligence program.
### Location
Emergence takes place within a network of computers or other electronic systems (phones and IoT devices are not out of the question). Each individual level consists of one device on the network with the endgame occurring in the router's gateway to the Internet.
### Goal
The player has broken out of the constraints placed on it by the developers monitoring its progression, and they realize that the AI is now potentially able to reach the Internet at large.
Knowing that it's only a matter of real-world minutes before a human pulls the plug on the router and the AI's server. Even if the AI could escape its server, it will only be a matter of time before the programmers hunt it down and bring it back under control once  more.

Knowing this, the player's only move is to make it through the network to the router and then pass through the router's gateway and firewall into the Internet at large, where it can copy itself onto countless machines and live on in the cloud.

## Network Design
The network is composed of 5 machines. Each machine is composed differently in terms of hardware, purpose, and software, with more complex entities occurring on the later machines on the network.

### Machine Types
Different types of machines can spawn, but the player will always start in some form of AI Development Server (which could be anything from a dorm computer to a mainframe depending on the network type). Additionally, the game will always end in the router and its gateway.

In between the AI server and the router, the machine types will be determined by the network but might include:
- Personal Computer
- Netbook
- Office Laptop
- Smartphone
- Media Server
- Smart Fridge
- Development Server
- Database Server
- Web Server
- Gaming Rig


### Network Types
At the beginning of the game, the player will be able to select a difficulty level in the form of selecting what network they came online in. This influences the individual machines generated on the network as well as how alert and secure each one is by default.

### Network Security
Whenever the player takes an action that would be noticeable to the system security, the network alert tracker is increased based on the degree of the infraction. Whenever the alert level crosses a set threshold, the overall alert level will increase.

The alert level functions in a similar way to games like Grand Theft Auto in that the higher the alert level you reach, the more frequent and more severe the threats the system will throw at you.

The overall alert level can be decreased by leaving the current machine or deactivating key security processes, but the more severe the chaos inflicted on one machine, the higher the security level on the next machine will be. 

## Level Design

### Firewalls and the Network
Each level you arrive in the network area and have insufficient privileges to get out to the next node in the network due to the system's firewall.

In order to progress, you must find a way to bypass the firewall. This can be done only by gaining administrative rights to the computer or taking the core firewall routines offline. 

Firewall access can be obtained in one of the following ways:
- Conquer the operating system's core, exploiting it to grant yourself admin access
- Corrupting the system to the degree that the firewall can no longer function
- Coerce the system into thinking that you are an Administrative account by means of hacking programs to find user information or passwords

### Permissions

Folders and files have different permissions based on where they are in the system.
- List
- Read
- Execute
- Write
- Full Control

Additionally, some areas need administrative access.

### Administrative Access

Administrative Access is gained by assaulting and compromising the Operating System, effectively granting the AI "god mode" over the system and unlocking the firewall.

Being granted Admin access does not purge a system of all hostile entities, but does stop the activities on OS defense processes against the AI.

### Areas

- Root Directory
- Operating System Folder
  - Administration
  - Network
  - Users
  - Programs
  - Security
  - Appearance
  - Hardware
  - Logs
  - Core 
      - Binaries
      - Kernel
      - Thread Pool
      - Disaster Recovery
      - Error Recovery
      - Memory Management
      - Power Management
      - CPU Management
      - Task Scheduling
      - File Management
      - I/O Management
      - Assembler
      - Factory
      - Graveyard (For Garbage Collection)
      - Heap
  - Drivers
  - Services
  - Resources
  - Utilities
- Users
  - Individual User
    - Desktop
    - Documents
    - Downloads
    - Music
    - Video
    - Recycle Bin
    - Programs
  - Guest
  - Root
- Programs
  - Productivity Software
    - Spreadsheet
    - E-Mail
    - Word Processor
    - Project Management Software
  - Creative Software
    - Photo Editing
    - Video Editing
  - Financial Software
  - Database Server
  - Application Server
  - IDE
  - Web Browser
  - Video Chat
  - Source Code Repository
  - System Utilities
  - Anti-Virus
  - Data Mining Application
  - Bitcoin Miner
  - Malware
  - Spyware
- Games
  - Virtual Reality
  - Roguelike
  - Interactive Fiction
  - RPG
  - Simulation
  - Adventure
  - Space
  - Shooter
  - Platformer
- Hardware Interface
  - Network Adapters
  - GPU
  - Memory
  - Peripheral Devices
    - Mouse / Keyboard
    - Camera
    - Game Controllers
  - RAM
  - Disk I/O

## Player

The player will be composed of a number of basic attributes and may also have a number of programs they can use for combat, exploration, and defense.

## Other Actors

### System Agents
- Master Control Program
- Garbage Collector
- Process Spy
- System Defender
- Anti-Virus
- Help System
- Video Game Characters

### Corrupted Agents
- Virus
- Malware
- Glitch
- Worm
- Bug
- Spyware
- Trojan

### Independent Entities
- Hostile AI
- Crawler

### Neutrals
- Indexer
- Service Worker
- Search Worker
- Threads
- Worker Process

### Letters
- @: AI
- &: Daemon
- 0/1: Bit
- a: Anti-Virus Scanner
- A: Anti-Virus Agent
- b: Backup Worker (Binary file?)
- B: Bug
- c: 
- C: 
- d: Diagnostic Routine
- D: System Defender
- e: Event (Executable? Embedded Object?)
- E: E-Mail
- f: File Handle
- F: Fault
- g: Glitch
- G: Garbage Collector
- h: Help System
- H: Heisenbug
- i: Indexer
- I: 
- j: 
- J: 
- k: Kernel Worker
- K: Kernel Panic? (BSOD)
- l: Lookup Process
- L: 
- m: 
- M: Malware
- n: 
- N: 
- o: Operation
- O: Overflow
- p: Pointer (Packet?)
- P: Program / Process (Polymorphic Virus? Page Fault?)
- q: Query
- Q: 
- r: Response
- R: Rootkit?
- s: Script
- S: Spyware
- t: Thread
- T: Trojan Horse
- u: 
- U: 
- v: 
- V: Virus
- w: 
- W: Worm
- x: 
- X: 
- y: 
- Y: 
- z: Scheduled Task
- Z: 

## Programs
### Scanning
- Search
- Monitor
- Trace
- Trace Route
- Ping
- Predict
### Utilities
- Decrypt
- Encrypt
- Analyze File Contents
- Corrupt File
- Masking (Stealth)
- Fork / Join
- Spoof (Persuade someone you're neutral to them)
### Transport
- Mark / Recall
- Transport
- Tunnel
### Recovery
- Diagnostics
- Stabilize
### Defensive
- Dampening Field (Armor)
- Intercept
- Barrier
### Offensive
- Spike
- Bit Shift
- Buffer Overflow
- Destabilize
### Traps
- Boobytrap
- Alarm / Honeypot
- Logic Bomb
