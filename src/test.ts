/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

// This file is required by karma.conf.js and loads recursively all the .spec and framework files

import { getTestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import 'zone.js/dist/async-test';
import 'zone.js/dist/fake-async-test';
import 'zone.js/dist/long-stack-trace-zone';
import 'zone.js/dist/proxy.js';
import 'zone.js/dist/sync-test';
import 'zone.js/dist/jasmine-patch';

/*
import '../node_modules/zone.js/dist/zone.js';
import '../node_modules/zone.js/dist/async-test.js';
import '../node_modules/zone.js/dist/fake-async-test.js';
import '../node_modules/zone.js/dist/long-stack-trace-zone.js';
import '../node_modules/zone.js/dist/proxy.js';
import '../node_modules/zone.js/dist/sync-test.js';
import '../node_modules/zone.js/dist/jasmine-patch.js';
// import '../node_modules/es6-shim/es6-shim.js';
// import '../node_modules/reflect-metadata/Reflect.js';
*/

// Unfortunately there's no typing for the `__karma__` variable. Just declare it as any.
declare const __karma__: any;
declare const require: any;

// Prevent timeouts on slower machines
jasmine.DEFAULT_TIMEOUT_INTERVAL = 3000000;

// Prevent Karma from running prematurely.
__karma__.loaded = (): void => {};

// First, initialize the Angular testing environment.
getTestBed().initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
// Then we find all the tests.
const context: any = require.context('./', true, /\.spec\.ts$/);
// And load the modules.
context.keys().map(context);
// Finally, start Karma to run the tests.
__karma__.start();
