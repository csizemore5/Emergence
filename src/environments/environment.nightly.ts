/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

export const environment: any = {
  production: false,
  enableAnalytics: true,
  allowDebugMode: true,
  environmentName: 'Nightly Build',
  isHostedContent: false,
  isTestRun: false,
  version: 'v{BUILD_VERSION} (Nightly)',
  traceAngularRoutes: true,
  baseUrl: 'https://emergenceapidev.azurewebsites.net'
};
