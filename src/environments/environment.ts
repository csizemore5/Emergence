/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment: any = {
  production: false,
  enableAnalytics: false,
  allowDebugMode: true,
  environmentName: 'Dev',
  isHostedContent: false,
  isTestRun: false,
  version: 'Dev Version',
  traceAngularRoutes: false,
  baseUrl: 'http://localhost:60751'
};
