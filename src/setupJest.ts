/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import 'jest-preset-angular';
import 'sortablejs';
import {environment} from './environments/environment';

environment.isTestRun = true;

const mock: any = (): any => {
  let storage: any = {};

  return {
    getItem: (key: any): any => (key in storage ? storage[key] : null),
    setItem: (key: any, value: any): any => (storage[key] = value || ''),
    removeItem: (key: any): any => delete storage[key],
    clear: (): any => (storage = {})
  };
};

Object.defineProperty(window, 'localStorage', { value: mock() });
Object.defineProperty(window, 'sessionStorage', { value: mock() });
Object.defineProperty(window, 'getComputedStyle', {
  value: (): string[] => ['-webkit-appearance']
});
