/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Component, OnInit } from '@angular/core';
import { Event, Router } from '@angular/router';
import { DialogService } from './services/ui-service/dialog.service';
import { TooltipService } from './services/ui-service/tooltip.service';
import { Logger } from './shared/utility/logger';

@Component({
  selector: 'em-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public title: string = 'Emergence';
  public panelHeight: number;
  public isFocused: boolean = false;
  public showDialog: boolean = false;

  constructor(private tooltipService: TooltipService, private route: Router, private dialogService: DialogService) {}

  public ngOnInit(): void {
    this.onResize();
    this.route.events.subscribe((e: Event): void => this.onRouterEvent(e));
  }

  public onResize(): void {
    this.panelHeight = window.innerHeight - 4;
  }

  public onWindowFocused(isFocused: boolean): void {
    this.isFocused = isFocused;
  }

  public onMainClick(): void {
    this.onWindowFocused(true);
  }

  public onMouseLeave(): void {
    this.tooltipService.cancelTooltip();
  }

  public onCloseDialogClick(event: MouseEvent): void {
    Logger.debug(`OnCloseClick`, event);
    this.dialogService.closeActiveDialog();
  }

  private onRouterEvent(e: any): void {
    this.showDialog = e && e.url && e.url.indexOf('dialog:') >= 0;
  }
}
