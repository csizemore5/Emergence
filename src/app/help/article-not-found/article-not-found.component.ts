/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'em-article-not-found',
  templateUrl: './article-not-found.component.html',
  styleUrls: ['./article-not-found.component.scss']
})
export class ArticleNotFoundComponent implements OnInit {
  constructor() {}

  public ngOnInit(): void {}
}
