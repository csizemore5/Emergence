/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Logger } from '../../shared/utility/logger';
import { HelpService } from '../help.service';
import { IHelpCategory } from '../i-help-category';
import { IHelpTopic } from '../i-help-topic';

@Component({
  selector: 'em-help-category',
  templateUrl: './help-category.component.html',
  styleUrls: ['./help-category.component.scss']
})
export class HelpCategoryComponent implements OnInit {
  public category: IHelpCategory;

  constructor(private route: ActivatedRoute) {}

  public ngOnInit(): void {
    this.route.params.subscribe(() => this.onParamsChanges());
    this.onParamsChanges();
  }

  private onParamsChanges(): void {
    this.category = this.route.snapshot.data['category'];

    Logger.debug(`On Category Load`, this.category, this.category, this.route.snapshot.data);
  }

  public getRoute(topic: IHelpTopic): string[] {
    return HelpService.getRouterLink(topic);
  }
}
