/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { inject, TestBed } from '@angular/core/testing';

import { HelpService } from './help.service';

describe('HelpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HelpService]
    });
  });

  it('should be created', inject([HelpService], (service: HelpService) => {
    expect(service).toBeTruthy();
  }));
});
