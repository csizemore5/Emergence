/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Component, OnInit } from '@angular/core';
import { HelpService } from '../help.service';

@Component({
  selector: 'em-help-credits',
  templateUrl: './help-credits.component.html',
  styleUrls: ['./help-credits.component.scss']
})
export class HelpCreditsComponent implements OnInit {
  constructor(private helpService: HelpService) {}

  public ngOnInit(): void {
    this.helpService.selectedCategory = this.helpService.findCategory('general');
  }
}
