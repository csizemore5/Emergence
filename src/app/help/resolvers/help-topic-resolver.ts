/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Logger } from '../../shared/utility/logger';
import { HelpService } from '../help.service';
import { IHelpCategory } from '../i-help-category';
import { IHelpTopic } from '../i-help-topic';

@Injectable()
export class HelpTopicResolver implements Resolve<IHelpTopic> {
  constructor(private helpService: HelpService, private router: Router) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IHelpTopic> | Promise<IHelpTopic> | IHelpTopic {
    Logger.debug(route, state);

    // Grab the category
    const category: IHelpCategory = route.parent.data['category'];
    if (!category) {
      this.router.navigate(['/help', 'category-not-found']);

      return null;
    }

    const topicId: string = route.paramMap.get('topic').toLowerCase();

    // Grab the topic out of the category. This COULD just call helpService.findTopic, but this wouldn't restrict it to the category
    const topic: IHelpTopic | undefined = category.topics.find((t: IHelpTopic): boolean => t.id === topicId);

    if (!topic) {
      this.router.navigate(['/help', 'topic-not-found']);
    }

    return topic;
  }
}
