/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { isNullOrUndefined } from 'util';
import { Logger } from '../../shared/utility/logger';
import { HelpService } from '../help.service';
import { IHelpCategory } from '../i-help-category';

@Injectable()
export class HelpCategoryResolver implements Resolve<IHelpCategory> {
  constructor(private helpService: HelpService, private router: Router) {}

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<IHelpCategory> | Promise<IHelpCategory> | IHelpCategory {
    let id: string = route.paramMap.get('category');

    if (isNullOrUndefined(id)) {
      id = route.data['categoryId'];
    }

    if (isNullOrUndefined(id)) {
      id = route.routeConfig.path;
    }

    const category: IHelpCategory = this.helpService.findCategory(id);

    if (!category) {
      Logger.warn(`Could not resolve category ${id}`, route, state);

      this.router.navigate(['/help', 'category-not-found']);
    }

    this.helpService.selectedCategory = category;

    return category;
  }
}
