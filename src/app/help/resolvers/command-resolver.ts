/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ActorCommand } from '../../libraries/world/actors/commands/actor-command';
import { CommandLookupService } from '../../services/core-engine/command-lookup.service';
import { Logger } from '../../shared/utility/logger';
import { HelpService } from '../help.service';

@Injectable()
export class CommandResolver implements Resolve<ActorCommand> {
  constructor(private commandLookupService: CommandLookupService, private helpService: HelpService, private router: Router) {}

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<ActorCommand> | Promise<ActorCommand> | ActorCommand {
    const id: string = route.paramMap.get('command');

    const command: ActorCommand = this.commandLookupService.findCommand(id);

    if (!command) {
      Logger.warn(`Could not find command ${id} in command resolver`);
      this.router.navigate(['/help', 'article-not-found']);
    }

    this.helpService.selectedCategory = this.helpService.findCategory('commands');

    return command;
  }
}
