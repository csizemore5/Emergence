/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HelpService } from '../help.service';
import { ReleaseInfo } from '../release-info';
import { ReleaseNotesService } from '../release-notes.service';

@Injectable()
export class ReleaseInfoResolver implements Resolve<ReleaseInfo> {
  constructor(private releaseNotesService: ReleaseNotesService, private helpService: HelpService, private router: Router) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ReleaseInfo> | Promise<ReleaseInfo> | ReleaseInfo {
    const topicId: string = route.paramMap.get('version').toLowerCase();

    // Grab the topic out of the category. This COULD just call helpService.findTopic, but this wouldn't restrict it to the category
    const topic: ReleaseInfo = this.releaseNotesService.releases.find((r: ReleaseInfo): boolean => r.version.toLowerCase() === topicId);

    if (!topic) {
      this.router.navigate(['/help', 'not-found']);
    }

    this.helpService.selectedCategory = this.helpService.findCategory('releases');

    return topic;
  }
}
