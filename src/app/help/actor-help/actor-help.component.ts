/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ActorCommand } from '../../libraries/world/actors/commands/actor-command';
import { CommandSlot } from '../../libraries/world/actors/commands/command-slot';
import { IActorDefinition } from '../../libraries/world/actors/i-actor-definition';
import { CommandLookupService } from '../../services/core-engine/command-lookup.service';
import { ActorDefinitionService } from '../../services/generation-service/actor-definition.service';
import { HelpService } from '../help.service';

@Component({
  selector: 'em-actor-help',
  templateUrl: './actor-help.component.html',
  styleUrls: ['./actor-help.component.scss']
})
export class ActorHelpComponent implements OnInit {
  public actorDefinition: IActorDefinition;
  public readonly slots: CommandSlot[];

  constructor(
    private route: ActivatedRoute,
    private helpService: HelpService,
    private actorService: ActorDefinitionService,
    private commandLookupService: CommandLookupService
  ) {
    this.slots = [];
  }

  public ngOnInit(): void {
    this.route.params.subscribe(() => this.onParamsChanges());
    this.helpService.selectedCategory = this.helpService.findCategory('actors');
    this.onParamsChanges();
  }

  private onParamsChanges(): void {
    this.slots.length = 0;

    this.actorDefinition = this.route.snapshot.data['actor'];

    // Set up the commands
    if (this.actorDefinition && this.actorDefinition.commands) {
      for (const commandKey of this.actorDefinition.commands) {
        const command: ActorCommand | null = this.commandLookupService.findCommand(commandKey);

        if (command) {
          this.slots.push(new CommandSlot(true, command));
        }
      }
    }
  }

}
