/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { MaterialColor } from '../libraries/material-color.enum';
import { StringHelper } from '../shared/utility/string-helper';
import { HelpService } from './help.service';
import { IHelpCategory } from './i-help-category';
import { IHelpTopic } from './i-help-topic';

/**
 * An Angular directive that takes in a string of content and parses out terms inside of brackets, treating them as links to new topics.
 */
@Directive({
  selector: '[emHelpLink]'
})
export class HelpLinkDirective implements OnInit {
  @Input('emHelpLink') public helpContent: string = '';

  constructor(private el: ElementRef, private helpService: HelpService) {}

  /**
   * Responds to the Angular onInit event. It's important to look at helpContent here as it may not be set during construction.
   */
  public ngOnInit(): void {
    this.parseText(this.helpContent);
  }

  /**
   * Parses the input text and adds spans to the native element this directive decorates, replacing bracketed terms with router links.
   * @param {string} input the text to translate
   */
  private parseText(input: string): void {
    if (!input) {
      return;
    }

    // Declare loop variables
    let remaining: string = input;
    let startIndex: number = remaining.indexOf('[');
    let endIndex: number = remaining.indexOf(']');

    /* We're going to loop through and find the first bracketed set of terms, then transform those to links. Items before this will be treated
       as spans, and items afterwards will be re-evaluated for bracketed terms. If no remaining brackets are detected (or no brackets are present)
       the remaining text will be output as a span.
     */
    do {
      startIndex = remaining.indexOf('[');
      endIndex = remaining.indexOf(']');
      if (startIndex >= 0 && endIndex > startIndex) {
        this.addSpanText(remaining.slice(0, startIndex));

        const substring: string = remaining.slice(startIndex + 1, endIndex);
        this.addHelpLink(substring);

        remaining = remaining.slice(endIndex + 1);
      }
    } while (startIndex >= 0 && endIndex > startIndex);

    if (remaining) {
      this.addSpanText(remaining);
    }
  }

  /**
   * Adds a span tag to the document with the specified text
   * @param {string} text the text to include in the span.
   */
  private addSpanText(text: string): void {
    if (!text) {
      return;
    }

    const span: HTMLSpanElement = document.createElement('span');
    span.innerText = text;
    this.el.nativeElement.appendChild(span);
  }

  /**
   * Builds a span tag for the given help text that will navigate to the help topic when clicked. This will be a span tag
   * styled as an anchor tag.
   * @param {string} text the text to display in the link.
   */
  private addHelpLink(text: string): void {
    if (!text) {
      return;
    }

    let displayText: string = text;
    let linkUrl: string = text;

    // Allow for pipe separating topics so that you can use custom display text with a set link like [path|display text].
    const pipeIndex: number = text.indexOf('|');
    if (pipeIndex >= 0 && text.length > pipeIndex) {
      displayText = text.substring(pipeIndex + 1);
      if (pipeIndex > 0) {
        linkUrl = text.substring(0, pipeIndex);
      } else {
        linkUrl = displayText;
      }
    }

    // Replace spaces with dashes
    linkUrl = StringHelper.replaceAll(linkUrl, ' ', '-');

    // Figure out what we're talking about. If it's an article or a category, we can link to it.
    const target: IHelpTopic | IHelpCategory = this.helpService.findTopicOrCategory(linkUrl);

    // Build and add the clickable element
    const link: HTMLElement = document.createElement('span');
    link.style.cursor = 'pointer';

    // Go with a different color to highlight when a link could not be found
    link.style.color = target ? MaterialColor.orange : MaterialColor.red;

    link.title = `View help on '${displayText}'`;
    link.innerText = displayText;
    link.onclick = (): void => this.onLinkClick(linkUrl);

    this.el.nativeElement.appendChild(link);
  }

  /**
   * Responds to a help topic link click event by triggering an Angular routing event.
   * @param {string} topicId the ID of the help topic that was clicked.
   */
  private onLinkClick(topicId: string): void {
    this.helpService.viewTopic(topicId);
  }
}
