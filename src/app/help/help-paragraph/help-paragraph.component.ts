/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'em-help',
  templateUrl: './help-paragraph.component.html',
  styleUrls: ['./help-paragraph.component.scss']
})
export class HelpParagraphComponent implements OnInit {
  @Input() public content: string;

  constructor() {}

  public ngOnInit(): void {}
}
