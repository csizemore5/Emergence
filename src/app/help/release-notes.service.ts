/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { EventEmitter, Injectable } from '@angular/core';
import { ReleaseInfo } from './release-info';

@Injectable()
export class ReleaseNotesService {
  public releases: ReleaseInfo[] = [];

  constructor() {
    this.loadReleases();
  }

  public releasesChanged: EventEmitter<ReleaseInfo[]> = new EventEmitter<ReleaseInfo[]>();

  private loadReleases(): void {
    this.releases = require('json-loader!App/Help/release-notes.json');
    this.releasesChanged.emit(this.releases);
  }

  public getNextRelease(release: ReleaseInfo): ReleaseInfo | null {
    if (!release) {
      return null;
    }

    const index: number = this.releases.indexOf(release);
    if (index <= 0) {
      return null;
    }

    return this.releases[index - 1];
  }

  public getPreviousRelease(release: ReleaseInfo): ReleaseInfo | null {
    if (!release) {
      return null;
    }

    const index: number = this.releases.indexOf(release);
    if (index < 0 || index >= this.releases.length - 1) {
      return null;
    }

    return this.releases[index + 1];
  }
}
