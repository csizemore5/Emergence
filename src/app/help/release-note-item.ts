export class ReleaseNoteItem {
  public header: string;
  public body: string[] = [];
  public listItems: string[];

  constructor(header: string, body: string) {
    this.header = header;
    this.body.push(body);
  }

  public addListItem(item: string): void {
    if (!this.listItems) {
      this.listItems = [];
    }
    this.listItems.push(item);
  }
}
