/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { EventEmitter, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { isNullOrUndefined } from 'util';
import { IActorDefinition } from '../libraries/world/actors/i-actor-definition';
import { CommandLookupService } from '../services/core-engine/command-lookup.service';
import { GameSetupService } from '../services/core-engine/game-setup.service';
import { ActorDefinitionService } from '../services/generation-service/actor-definition.service';
import { Logger } from '../shared/utility/logger';
import { HelpTopicType } from './help-topic-type.enum';
import { IHelpCategory } from './i-help-category';
import { IHelpTopic } from './i-help-topic';
import { ReleaseNotesService } from './release-notes.service';

@Injectable()
export class HelpService {
  public categories: IHelpCategory[] = [];
  public selectedCategoryChanged: EventEmitter<IHelpCategory>;
  private _selectedCategory: IHelpCategory;

  constructor(
    private releaseNotesService: ReleaseNotesService,
    private actorDefinitionService: ActorDefinitionService,
    private gameSetupService: GameSetupService,
    private commandLookupService: CommandLookupService,
    private router: Router
  ) {
    this.selectedCategoryChanged = new EventEmitter<IHelpCategory>();
    this.loadCategories();
  }

  public static getRouterLink(category: IHelpTopic | IHelpCategory): string[] {
    if (isNullOrUndefined((<IHelpCategory>category).topics)) {
      return ['/help', HelpService.getCategoryRoute(category.type), category.id];
    } else {
      return ['/help', HelpService.getCategoryRoute(category.type)];
    }
  }

  get selectedCategory(): IHelpCategory {
    return this._selectedCategory;
  }

  set selectedCategory(value: IHelpCategory) {
    if (this._selectedCategory !== value) {
      this._selectedCategory = value;
      Logger.debug(`Setting help category`, value);
      this.selectedCategoryChanged.emit(value);
    }
  }

  public findCategory(id: string): IHelpCategory {
    return this.categories.find((c: IHelpCategory): boolean => c.id === id);
  }

  private static getCategoryRoute(type: HelpTopicType): string {
    switch (type) {
      case HelpTopicType.general:
        return 'general';

      case HelpTopicType.releaseNotes:
        return 'releases';

      case HelpTopicType.environment:
        return 'environment';

      case HelpTopicType.player:
        return 'players';

      case HelpTopicType.otherActor:
        return 'actors';

      case HelpTopicType.commands:
        return 'commands';

      default:
        throw new Error(`Help topic type ${type} did not map properly`);
    }
  }

  public findTopicOrCategory(id: string): IHelpTopic | IHelpCategory {
    const lowerId: string = id.toLowerCase();

    for (const cat of this.categories) {
      if (cat.id.toLowerCase() === lowerId) {
        Logger.debug(`Find Topic or Category ${id} resolved to Category`, cat);

        return cat;
      }

      const topic: IHelpTopic = cat.topics.find((t: IHelpTopic): boolean => t.id.toLowerCase() === lowerId);
      if (topic) {
        Logger.debug(`Find Topic or Category ${id} resolved to Topic`, topic);

        return topic;
      }
    }

    return null;
  }

  public viewTopic(topicId: string): void {
    if (!topicId) {
      return;
    }

    const topic: IHelpTopic | IHelpCategory = this.findTopicOrCategory(topicId);

    if (topic) {
      const routerLink: string[] = HelpService.getRouterLink(topic);
      Logger.debug(`Navigating to topic ${topicId}`, routerLink);

      this.router.navigate(routerLink);
    } else {
      Logger.error(`Tried to navigate to topic ${topicId} but it could not be found.`);
    }
  }

  private loadCategories(): void {
    // Load the raw category data
    this.categories = require('json-loader!App/Help/help.json');

    // Dynamically populate categories
    this.buildCommandsCategory(this.categories.find((c: IHelpCategory): boolean => c.type === HelpTopicType.commands));
    this.buildPlayerCategory(this.categories.find((c: IHelpCategory): boolean => c.type === HelpTopicType.player));
    this.buildOtherActorsCategory(this.categories.find((c: IHelpCategory): boolean => c.type === HelpTopicType.otherActor));
    this.buildReleaseNotesCategory(this.categories.find((c: IHelpCategory): boolean => c.type === HelpTopicType.releaseNotes));

    Logger.debug(`After category load, category contents are`, this.categories);
  }

  private addTopic(category: IHelpCategory, topic: IHelpTopic): void {
    category.topics.push(topic);
  }

  private buildCommandsCategory(category: IHelpCategory): void {
    for (const command of this.commandLookupService.commands) {
      const releaseTopic: IHelpTopic = this.buildTopic(command.id, command.name, HelpTopicType.commands);

      this.addTopic(category, releaseTopic);
    }
  }

  private buildReleaseNotesCategory(category: IHelpCategory): void {
    for (const release of this.releaseNotesService.releases) {
      const releaseTopic: IHelpTopic = this.buildTopic(release.version, release.version, HelpTopicType.releaseNotes);

      this.addTopic(category, releaseTopic);
    }
  }

  private buildOtherActorsCategory(category: IHelpCategory): void {
    const actors: IActorDefinition[] = this.actorDefinitionService.getNonPlayerDefinitions();
    for (const playerDef of actors) {
      this.addTopic(category, this.buildTopic(playerDef.id, playerDef.name, HelpTopicType.otherActor));
    }
  }

  private buildPlayerCategory(category: IHelpCategory): void {
    for (const playerDef of this.gameSetupService.availableClasses) {
      this.addTopic(category, this.buildTopic(playerDef.playerClass, playerDef.shortName, HelpTopicType.player));
    }
  }

  private buildTopic(id: string, name: string, type: HelpTopicType): IHelpTopic {
    return {
      id,
      name,
      type,
      body: []
    };
  }
}
