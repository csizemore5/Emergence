import { HelpTopicType } from './help-topic-type.enum';
import { IHelpBody } from './i-help-body';

export interface IHelpTopic {
  id: string;
  name: string;
  type: HelpTopicType;
  body: IHelpBody[];
}
