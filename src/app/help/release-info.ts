import { ReleaseNoteItem } from './release-note-item';

export class ReleaseInfo {
  public version: string;
  public date: Date;
  public summary: string;

  public features: ReleaseNoteItem[] = [];
  public bugfixes: ReleaseNoteItem[] = [];
  public notes: ReleaseNoteItem[] = [];

  constructor(version: string, date: Date, summary: string) {
    this.version = version;
    this.date = date;
    this.summary = summary;
  }

  public addFeature(item: ReleaseNoteItem): void {
    this.features.push(item);
  }

  public addBugfix(item: ReleaseNoteItem): void {
    this.bugfixes.push(item);
  }

  public addImprovement(item: ReleaseNoteItem): void {
    this.bugfixes.push(item);
  }

  public addNote(item: ReleaseNoteItem): void {
    this.notes.push(item);
  }
}
