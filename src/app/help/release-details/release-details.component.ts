/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ReleaseInfo } from '../release-info';
import { ReleaseNotesService } from '../release-notes.service';

@Component({
  selector: 'em-release-details',
  templateUrl: './release-details.component.html',
  styleUrls: ['./release-details.component.scss']
})
export class ReleaseDetailsComponent implements OnInit {
  public release: ReleaseInfo;
  public nextRelease: ReleaseInfo;
  public previousRelease: ReleaseInfo;

  constructor(private route: ActivatedRoute, private releaseNotesService: ReleaseNotesService) {}

  public ngOnInit(): void {
    this.route.params.subscribe(() => this.onParamsChanges());
    this.onParamsChanges();
  }

  private onParamsChanges(): void {
    this.release = this.route.snapshot.data['release'];
    this.nextRelease = this.releaseNotesService.getNextRelease(this.release);
    this.previousRelease = this.releaseNotesService.getPreviousRelease(this.release);
  }

  public getReleaseVersion(r: ReleaseInfo): string {
    return r.version;
  }
}
