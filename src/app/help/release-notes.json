[
  {
    "features": [
      {
        "header": "Five Total Levels",
        "body": [
          "The game now plays over the course of five pre-fabricated levels instead of the standard one, with victory at the end of the fifth level."
        ]
      },
      {
        "header": "Random Core Placement",
        "body": [
          "Cores are now more likely to be placed randomly throughout the level instead of isolated in the operating system kernel room."
        ]
      },
      {
        "body": [
          "Got rid of diagonal movement via the keyboard and mouse input. This was done to make combat navigation more interesting, allow playing more evenly on smaller keyboards and laptops, and to reduce some of the decisions tactical AI will need to make down the road once that is implemented. This movement restriction is an experiment at the moment, but my hope is that it will add depth to the game's combat down the road."
        ],
        "header": "Removed Diagonal Movement"
      },
      {
        "header": "Tweaked Encounters",
        "body": [
          "Individual encounters available in each room have been tweaked significantly. You'll see different distributions and configurations of enemies after this change. Some enemies may now be more or less common than they were before. Expect further tweaks to be ongoing as development continues."
        ]
      },
      {
        "header": "Teleport Damage",
        "body": [
          "Teleporting onto another entity will now damage both you and the target, while still swapping locations."
        ]
      }
    ],
    "bugfixes": [
      {
        "header": "Teleporting Items",
        "body": [
          "The swap command will now swap pick-ups with the player if the player tries to swap onto the spot of a pickup."
        ]
      }
    ],
    "notes": [
      {
        "body": [
          "Level generation logic is now accomplished via a Web API. This is in preparation to move additional logic to the C# programming language in the goal of moving to Unity once the prototype is complete."
        ],
        "header": "Level Generation Changes"
      },
      {
        "body": [
          "Data for segments of individual levels, level arrangement, and available encounters are now stored in a data files. This helps with readability, extensibility, and moves the app closer to dynamically generated levels."
        ],
        "header": "Data-Driven Game World"
      }
    ],
    "version": "v0.3.3",
    "date": "2018-07-14T04:00:00.000Z",
    "summary": "More Levels, Significantly Reworked Technology"
  },
  {
    "features": [
      {
        "body": [
          "A variety of help content has been added on core game concepts, enemies, and other topics."
        ],
        "header": "Help Content"
      },
      {
        "body": [
          "The help system now contains a sidebar for navigation and is dynamically populated by the content within the game. While help content is still fairly minimal, this should ensure that stats and entities are automatically accurate with each release."
        ],
        "header": "Dynamic Help System"
      }
    ],
    "bugfixes": [
      {
        "body": [
          "Itch.io app would still launch on a 'not found' page in some circumstances"
        ],
        "header": "Itch.io Windows App Startup Page Fix"
      },
      {
        "body": [
          "Tooltips for tiles in the game world no longer are displayed when you move via keyboard controls. Tooltips are only triggered by actual mouse movement."
        ],
        "header": "Better Tooltips"
      },
      {
        "body": [
          "Better small resolution support for the new game pages."
        ],
        "header": "New Game Display on Small Resolutions"
      }
    ],
    "notes": [
      {
        "body": [
          "[releases|Release notes] are dynamically displayed based on the contents of a data file. This should make it easier to reformat and manipulate release notes in the future. Very meta."
        ],
        "header": "Dynamic Release Notes"
      },
      {
        "body": [
          "Page navigation buttons now have icons associated with them for a better user experience."
        ],
        "header": "Icons on buttons"
      }
    ],
    "version": "v0.3.2",
    "date": "2018-04-23T04:00:00.000Z",
    "summary": "Introduces a dynamic help system as well as a number of bugfixes."
  },
  {
    "features": [],
    "bugfixes": [
      {
        "body": [
          "When running the Itch.io app and launching version 0.3.0 of the application, the app would start on a \"Not Found\" page with a link to the main menu. This has been fixed."
        ],
        "header": "Fixed Itch.io Desktop Launch Page"
      }
    ],
    "notes": [],
    "version": "v0.3.1",
    "date": "2018-04-04T04:00:00.000Z",
    "summary": "A minor bugfix update."
  },
  {
    "features": [
      {
        "body": [
          "Switched to a darker and lower resolution tileset and sprite sheet for better maintainability and a more appropriate thematic feel. All art is likely placeholder content until the end of the project."
        ],
        "header": "New Graphics"
      },
      {
        "body": [
          "You can now select a character class on startup. Character classes are not final by any means. Expect more featuresto this system, new stats, changed stats, new classes, renamed or removed classes, etc. This is simply a starting point for being able to differentiate between characters on game start and experiment with encouraging different playstyles."
        ],
        "header": "Character Selection"
      },
      {
        "body": [
          "The command management screen now sports a more intelligent arrangement system that can do more with the available space on the page at smaller resolutions. This is particularly useful when hosted in a web page in non-fullscreen mode on Itch.io or similar."
        ],
        "header": "Smarter Resizing"
      },
      {
        "body": [
          "The game now starts on a main menu with a title screen. This will be a launch point for game loading, managing game options, and exploring the help system once those features are implemented."
        ],
        "header": "Added Main Menu"
      }
    ],
    "bugfixes": [
      {
        "body": [
          "Removed the submit feedback / bug feature as it was hard-coded to an unreliable IP address. If the address of the target machine changed, pages experienced a long delay during load."
        ],
        "header": "Improved Startup Time"
      },
      {
        "body": [
          "The actor previously known as Help System is now known as Helpy. This actor now has 1 hit point instead of 2, a lower evasion attribute, and no damage resistance, meaning that it's now substantially easier to kill it."
        ],
        "header": "Nerfed 'Helpy'"
      },
      {
        "body": [
          "In response to early user feedback, a few objects have been renamed for better thematic fit. These names are not final."
        ],
        "header": "Immersion Improvements",
        "listItems": [
          "Cabling is now Pipeline",
          "Walkway is now Queue",
          "Doors are now Access Ports",
          "Empty tiles are now Unallocated Space"
        ]
      },
      {
        "body": [
          "The game's URL updates itself on screen navigation. This doesn't impact most users via Itch.io or those playing via IFrames, but those playing the hosted or development versions will be able to look at the current screen, refresh the browser, etc. and maintain some semblance of where they left off (though game state is not preserved on refresh yet)"
        ],
        "header": "URL Addressable Pages"
      },
      {
        "body": [
          "The Game Over screen is styled to match the main menu and character selection screens. This is nice because you're going to see this screen a lot."
        ],
        "header": "Game Over Screen Styling"
      }
    ],
    "notes": [
      {
        "body": [
          "The help system has been reworked to be accessible from both the main menu and from inside of the game. Additionally, the system is being built in such a way that it can be expanded to serve as a more robust almost wiki-like help system with links to various topics and dynamically-generated indexes. Of course, that doesn't matter yet since those features aren't present, but some day they'll be there and it'll be sweet."
        ],
        "header": "Help System Foundations"
      },
      {
        "body": [
          "The existing game engine's interaction between the HTML view content and the game engine has been reworked to take advantage of the Angular 5 framework and some of the architectural improvements that leads to in terms of designing a number of smaller pages that interact with the same data services."
        ],
        "header": "Angular Architecture"
      },
      {
        "body": [
          "The game now uses Google Analytics to monitor which pages in the game menu that people visit the most along with what characters are selected and what error messages that are logged to the console. This will be helpful for maintenance purposes and determining what features users might have missed, if any characters are not getting the necessary attention, etc. This feature will be disable-able once an options menu is implemented and primarily consists of information about the version of the web browser, operating system, language settings, and city that the user is playing from."
        ],
        "header": "Google Analytics Improvements"
      }
    ],
    "version": "v0.3.0",
    "date": "2018-04-03T04:00:00.000Z",
    "summary": "A major update containing new graphics, character selection, and game menus."
  }
]
