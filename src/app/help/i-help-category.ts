import { HelpTopicType } from './help-topic-type.enum';
import { IHelpBody } from './i-help-body';
import { IHelpTopic } from './i-help-topic';

export interface IHelpCategory {
  id: string;
  header: string;
  topics: IHelpTopic[];
  type: HelpTopicType;
  body: IHelpBody[];
}
