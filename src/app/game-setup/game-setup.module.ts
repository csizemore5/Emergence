/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ClassDetailsComponent } from './class-details/class-details.component';
import { GameSetupComponent } from './game-setup.component';

@NgModule({
  imports: [CommonModule, SharedModule],
  declarations: [GameSetupComponent, ClassDetailsComponent]
})
export class GameSetupModule {}
