import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HelpService } from '../../help/help.service';
import { CharacterClass } from '../../libraries/setup/character-class';
import { ActorCommand } from '../../libraries/world/actors/commands/actor-command';
import { CommandSlot } from '../../libraries/world/actors/commands/command-slot';
import { IActorDefinition } from '../../libraries/world/actors/i-actor-definition';
import { CommandLookupService } from '../../services/core-engine/command-lookup.service';
import { GameSetupService } from '../../services/core-engine/game-setup.service';
import { ActorDefinitionService } from '../../services/generation-service/actor-definition.service';
import { Logger } from '../../shared/utility/logger';

@Component({
  selector: 'em-class-details',
  templateUrl: './class-details.component.html',
  styleUrls: ['./class-details.component.scss']
})
export class ClassDetailsComponent implements OnInit {
  public characterClass: CharacterClass;
  public actorDefinition: IActorDefinition;
  public readonly slots: CommandSlot[];

  constructor(
    private route: ActivatedRoute,
    private setupService: GameSetupService,
    private helpService: HelpService,
    private actorService: ActorDefinitionService,
    private commandLookupService: CommandLookupService
  ) {
    this.slots = [];
  }

  public ngOnInit(): void {
    this.route.params.subscribe(() => this.onParamsChanges());
    this.helpService.selectedCategory = this.helpService.findCategory('players');
    this.onParamsChanges();
  }

  private onParamsChanges(): void {
    this.slots.length = 0;

    const routeData: CharacterClass = this.route.snapshot.data['classId'];

    this.characterClass = routeData;

    this.actorDefinition = this.characterClass ? this.actorService.getActorDefinition(this.characterClass.playerClass) : null;

    // Set up the commands
    if (this.actorDefinition) {
      for (const commandKey of this.actorDefinition.commands) {
        const command: ActorCommand | null = this.commandLookupService.findCommand(commandKey);

        if (command) {
          this.slots.push(new CommandSlot(true, command));
        }
      }
    }

    Logger.debug(`Evaluating route data on Class Details`, routeData, this.actorDefinition);

    this.setupService.selectedClass = this.characterClass;
  }
}
