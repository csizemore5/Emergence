/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {inject, TestBed} from '@angular/core/testing';

import {ActorKnowledgeService} from './actor-knowledge.service';

describe('ActorKnowledgeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActorKnowledgeService]
    });
  });

  it('should be created', inject([ActorKnowledgeService], (service: ActorKnowledgeService) => {
    expect(service).toBeTruthy();
  }));
});
