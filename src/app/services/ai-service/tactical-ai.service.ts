/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Injectable } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { Randomizer } from '../../libraries/randomizer';
import { Actor } from '../../libraries/world/actors/actor';
import { GameCommand } from '../../libraries/world/actors/commands/game-command.enum';
import { Cell } from '../../libraries/world/cell';
import { Logger } from '../../shared/utility/logger';
import { ICommandHandler } from '../core-engine/i-command-handler';
import { WorldService } from '../core-engine/world.service';

@Injectable()
export class TacticalAiService {
  private logEvaluation: boolean = false;

  constructor(private worldService: WorldService) {}

  public performActorTurn(actor: Actor, commandHandler: ICommandHandler): boolean {
    // Sanity check
    if (!actor || actor.isDead()) {
      return;
    }

    let cells: Cell[] = this.worldService.getCellGrid(actor.pos, true, 1, false);

    // Only evaluate cells with actors who are hostile.
    cells = cells.filter((c: Cell): boolean => !c.actor || c.actor === actor || c.actor.isHostileTo(actor, this.worldService));

    let bestCell: Cell;
    let bestScore: number = 0;

    // Don't allow actors to consider walking through walls or doors. Constrain to their source room for now.
    const candidates: Cell[] = cells.filter(
      (cf: Cell): boolean => !isNullOrUndefined(cf.actor) || (!cf.isImpassible && cf.allowAiMovement(actor))
    );

    if (this.logEvaluation) {
      Logger.debug(`Evaluating cells for ${actor.name}`);
    }

    for (const c of candidates) {
      const score: number = this.getMoveScore(actor, c);
      if (!bestCell || score > bestScore) {
        bestScore = score;
        bestCell = c;
      }
    }

    if (this.logEvaluation) {
      Logger.debug(`Best cell score: ${bestScore}`, bestCell);
    }

    if (bestCell && !bestCell.pos.equals(actor.pos)) {
      if (this.logEvaluation) {
        Logger.debug(`${actor.name} executes command at ${bestCell.pos.toString()}`);
      }

      return commandHandler.handleCommand(actor, GameCommand.move, bestCell.pos);
    } else {
      if (this.logEvaluation) {
        Logger.debug(`${actor.name} lurks...`);
      }

      return commandHandler.handleCommand(actor, GameCommand.wait);
    }
  }

  private getMoveScore(actor: Actor, cell: Cell): number {
    const hasAnotherActor: boolean = cell.actor && cell.actor !== actor;
    const hasHostileActor: boolean = hasAnotherActor && actor.isHostileTo(cell.actor, this.worldService);

    // Immobile actors are highly prone to staying put
    if (actor.isImmobile) {
      if (actor.pos.equals(cell.pos)) {
        return 75;
      } else if (hasHostileActor) {
        return 500;
      } else {
        return -500;
      }
    }

    // Always attack if able and the target is hostile
    if (hasAnotherActor) {
      return hasHostileActor ? 500 : -500;
    }

    return Randomizer.getWholePercent(); // Completely random for now
  }
}
