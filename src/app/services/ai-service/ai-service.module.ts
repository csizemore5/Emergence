/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ActorKnowledgeService} from './actor-knowledge.service';
import {TacticalAiService} from './tactical-ai.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    ActorKnowledgeService,
    TacticalAiService
  ]
})
export class AiServiceModule {

}
