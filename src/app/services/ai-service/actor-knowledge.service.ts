/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Injectable } from '@angular/core';
import { Actor } from '../../libraries/world/actors/actor';
import { Point } from '../../libraries/world/point';
import { IInitializable } from '../i-initializable';

@Injectable()
export class ActorKnowledgeService implements IInitializable {
  private knownCells: any[];

  constructor() {
    this.knownCells = [];
  }

  public initialize(): void {
    this.knownCells = [];
  }

  public addKnownCells(actor: Actor, points: Point[]): void {
    if (!this.knownCells[actor.id]) {
      this.knownCells[actor.id] = new Set<string>();
    }

    const cellsSet: Set<string> = this.knownCells[actor.id];

    for (const point of points) {
      const key: string = `${point.x}_${point.y}`;
      if (!cellsSet.has(key)) {
        cellsSet.add(key);
      }
    }
  }

  public isKnownTo(actor: Actor, pos: Point): boolean {
    return this.knownCells[actor.id] && this.knownCells[actor.id].has(`${pos.x}_${pos.y}`);
  }
}
