import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observer } from 'rxjs/observer';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Logger } from '../shared/utility/logger';

@Injectable()
export class HttpService {
  public static baseUrl: string = environment.baseUrl;
  private client: HttpClient;

  constructor(client: HttpClient) {
    this.client = client;
  }

  public get(url: string): void {
    const fullUrl: string = `${HttpService.baseUrl}/${url}`;

    Logger.debug(`Beginning HttpGet invoke to ${fullUrl}`);

    return Observable.create((obs: Observer<any>) => {
      this.client.get(fullUrl).subscribe(
        (r: any) => {
          Logger.debug(`HttpGet operation to ${fullUrl} completed`, r);

          // Send the response along to the invoker
          obs.next(r);
          obs.complete();
        },
        (err: any) => {
          Logger.error(`Error on HttpGet invoke to ${fullUrl}`, err);

          // Pass the error along to the client observer
          obs.error(err);
        }
      );
    });
  }

  public post(url: string, content: any): Observable<any> {
    const fullUrl: string = `${HttpService.baseUrl}/${url}`;

    Logger.debug(`Beginning HttpPost invoke to ${fullUrl}`, content);

    return Observable.create((obs: Observer<any>) => {
      this.client.post(fullUrl, content).subscribe(
        (r: any) => {
          Logger.debug(`HttpPost operation to ${fullUrl} completed`, r);

          // Send the response along to the invoker
          obs.next(r);
          obs.complete();
        },
        (err: any) => {
          Logger.error(`Error on HttpPost invoke to ${fullUrl}`, err);

          // Pass the error along to the client observer
          obs.error(err);
        }
      );
    });
  }
}
