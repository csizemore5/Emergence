/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { EventEmitter, Injectable } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { environment } from '../../../environments/environment';
import { FovCalculator } from '../../libraries/fov-calculator';
import { GameStatus } from '../../libraries/game-status';
import { MaterialColor } from '../../libraries/material-color.enum';
import { ButtonData } from '../../libraries/rendering/button-data';
import { ButtonInstruction } from '../../libraries/rendering/button-instruction';
import { CellColorizer } from '../../libraries/rendering/cell-colorizer';
import { CellRenderInstruction } from '../../libraries/rendering/cell-render-instruction';
import { CommandButtonInstruction } from '../../libraries/rendering/command-button-instruction';
import { ICellInstructionGenerationContext } from '../../libraries/rendering/generation/i-cell-instruction-generation-context';
import { IGenerationContext } from '../../libraries/rendering/generation/i-generation-context';
import { HeaderInstruction } from '../../libraries/rendering/header-instruction';
import { ITooltipProvider } from '../../libraries/rendering/i-tooltip-provider';
import { LogRenderInstruction } from '../../libraries/rendering/log-render-instruction';
import { ProgressBarRenderInstruction } from '../../libraries/rendering/progress-bar-render-instruction';
import { RectangleInstruction } from '../../libraries/rendering/rectangle-instruction';
import { RenderInstruction } from '../../libraries/rendering/render-instruction';
import { TextAlignment } from '../../libraries/rendering/text-alignment';
import { TextRenderInstruction } from '../../libraries/rendering/text-render-instruction';
import { TooltipInfo } from '../../libraries/rendering/tooltip-info';
import { TooltipRenderInstruction } from '../../libraries/rendering/tooltip-render-instruction';
import { ActiveCommand } from '../../libraries/world/actors/commands/active-command';
import { ActorCommand } from '../../libraries/world/actors/commands/actor-command';
import { CommandSlot } from '../../libraries/world/actors/commands/command-slot';
import { GameCommand } from '../../libraries/world/actors/commands/game-command.enum';
import { PlayerActor } from '../../libraries/world/actors/player-actor';
import { Cell } from '../../libraries/world/cell';
import { Point } from '../../libraries/world/point';
import { Logger } from '../../shared/utility/logger';
import { ActorKnowledgeService } from '../ai-service/actor-knowledge.service';
import { CommandService } from '../core-engine/command.service';
import { GameLoopService } from '../core-engine/game-loop.service';
import { WorldService } from '../core-engine/world.service';
import { ButtonService } from '../ui-service/button.service';
import { MessageService } from '../ui-service/message.service';
import { RenderingService } from './rendering.service';

@Injectable()
export class RenderInstructionGenerationService implements ITooltipProvider {
  public instructionsGenerated: EventEmitter<RenderInstruction[]>;

  private readonly colorizer: CellColorizer;
  private instructions: RenderInstruction[];
  private context: IGenerationContext;

  private headerHeight: number = 42;
  private buttonSize: Point = new Point(56, 56);

  private tooltipInstructionId: string;

  constructor(
    private worldService: WorldService,
    private messageService: MessageService,
    private knowledgeService: ActorKnowledgeService,
    private renderingService: RenderingService,
    private gameLoopService: GameLoopService,
    private buttonService: ButtonService,
    private commandService: CommandService
  ) {
    this.instructionsGenerated = new EventEmitter<RenderInstruction[]>();
    this.colorizer = new CellColorizer();
    this.instructions = [];
  }

  public requestInstructions(context: IGenerationContext): void {
    this.context = context;

    this.instructions.length = 0;

    // Render the main UI
    this.renderCells();

    // Render the player's health, etc. on top of everything else at the end
    this.renderHud();

    this.instructionsGenerated.emit(this.instructions.splice(0));
  }

  public renderCells(): void {
    // If we're still loading, don't display anything
    if (!this.worldService.currentLevel) {
      Logger.debug(`Cannot render cells when current level is not set.`);

      return;
    }

    const screenPos: Point = new Point(0, 0);

    const fovCells: Cell[] = FovCalculator.calculateVisibleCellsForActor(this.worldService.player, this.worldService.currentLevel);
    this.knowledgeService.addKnownCells(this.worldService.player, fovCells.map((c: Cell): Point => c.pos));

    const isDebug: boolean = false; // this.gameLoopService.isDebugMode;

    for (let cellY: number = 0; cellY < this.context.maxRows; cellY++) {
      screenPos.x = 0;

      for (let cellX: number = 0; cellX < this.context.maxColumns; cellX++) {
        const pos: Point = new Point(cellX + this.context.cellOffset.x, cellY + this.context.cellOffset.y);

        const isVisible: boolean = fovCells.filter((c: Cell): boolean => c.pos.equals(pos)).length > 0;

        const cellContext: ICellInstructionGenerationContext = {
          cellPos: pos,
          colorizer: this.colorizer,
          screenPos: new Point(screenPos.x, screenPos.y),
          isDebugMode: isDebug,
          isInitialRender: this.context.isInitialRender,
          isVisible,
          isKnown: isVisible || this.knowledgeService.isKnownTo(this.worldService.player, pos),
          charSize: this.context.charSize,
          actor: this.worldService.player
        };
        this.addCellInstructions(cellContext);

        screenPos.x += this.context.charSize.x;
      }

      screenPos.y += this.context.charSize.y;
    }
  }

  /**
   * Generates rendering instructions for the user interface / heads up display.
   */
  public renderHud(): void {
    this.renderActorHud(this.context.actor);
    this.renderGameLog();
    this.renderPrompt();
    this.renderButtons();
    this.renderHeader();

    // Tooltips must render after all instructions they can possibly annotate
    this.renderTooltip();
  }

  public requestTooltip(instruction: RenderInstruction): void {
    if (instruction && !instruction.id) {
      throw new Error('Instruction ID must be provided when requesting a tooltip');
    }
    if (this.tooltipInstructionId !== instruction.id) {
      Logger.debug(`Setting tool tip instruction to ${instruction.id}`);
      this.tooltipInstructionId = instruction.id;
      this.renderingService.requestRender(false);
    }
  }

  public cancelTooltip(instruction: RenderInstruction = null): void {
    if (
      (isNullOrUndefined(instruction) && this.tooltipInstructionId !== null) ||
      (instruction !== null && this.tooltipInstructionId === instruction.id)
    ) {
      this.tooltipInstructionId = null;
      this.renderingService.requestRender(false);
    }
  }

  private addCellInstructions(context: ICellInstructionGenerationContext): void {
    // Figure out what to render at this location
    const cell: Cell = this.worldService.getCell(context.cellPos);

    // Sanity check - don't render a cell that has nothing there
    if (!context.isVisible && !context.isKnown) {
      return;
    }

    // This guy is going to do all of the heavy lifting
    const instr: CellRenderInstruction = new CellRenderInstruction(context, cell);
    instr.tooltip = context.colorizer.getCellTooltip(cell, context.isDebugMode, context.isVisible);

    if (this.gameLoopService.gameState === GameStatus.selectingTarget) {
      instr.clickFunction = (): boolean => this.commandService.specifyTargetForCommand(cell);
      instr.cursorColor =
        this.commandService.pendingCommand && !this.commandService.pendingCommand.checkValidTarget(cell)
          ? MaterialColor.green
          : MaterialColor.red;
    } else {
      if (cell.pos.isAdjacentTo(context.actor.pos, false)) {
        instr.clickFunction = (): boolean => this.commandService.handleCommand(this.worldService.player, GameCommand.move, cell.pos);
        instr.cursorColor = cell.actor && cell.actor !== this.worldService.player ? MaterialColor.deepOrange : MaterialColor.white;
      } else {
        instr.cursorColor = MaterialColor.grey;
      }
    }

    this.instructions.push(instr);
  }

  /**
   * Builds a button rendering instruction given the parameters and returns it.
   * @param {Point} pos The screen position the button should be displayed at.
   * @param {ButtonData} data The information about the button to generate a rendering instruction for.
   * @returns {ButtonInstruction}
   */
  private buildUiButton(pos: Point, data: ButtonData): ButtonInstruction {
    const instruction: ButtonInstruction = new ButtonInstruction(pos, this.buttonSize, MaterialColor.greyLight2);
    instruction.hoverColor = MaterialColor.white;
    instruction.pressedColor = MaterialColor.blueGreyLight2;

    instruction.id = `btn${data.tooltip.title}`;
    instruction.tooltip = data.tooltip;
    instruction.icon = data.icon;
    instruction.clickFunction = data.clickFunction;
    instruction.iconName = data.iconName;

    return instruction;
  }

  private renderGameLog(): void {
    const maxEntries: number = 5;

    const logs: string[] = this.messageService.logMessages;
    const messages: string[] = logs.slice(logs.length - Math.min(logs.length, maxEntries));
    const instruction: LogRenderInstruction = new LogRenderInstruction(
      new Point(4, 50),
      new Point(400, messages.length * 42),
      MaterialColor.greenAccent3,
      messages
    );

    this.instructions.push(instruction);
  }

  private renderTooltip(): void {
    let tooltipParent: RenderInstruction = null;
    if (this.tooltipInstructionId) {
      const matches: RenderInstruction[] = this.instructions.filter((i: RenderInstruction): boolean => i.id === this.tooltipInstructionId);
      if (matches && matches.length >= 1) {
        tooltipParent = matches[0];
      }
    }

    const instr: TooltipRenderInstruction = new TooltipRenderInstruction(
      tooltipParent,
      MaterialColor.blueGreyDark2,
      MaterialColor.blueGreyLight4
    );
    instr.renderParent = tooltipParent && tooltipParent.tooltip && tooltipParent.tooltip.renderThumbnail;
    instr.id = 'Tooltip';
    this.instructions.push(instr);
  }

  private renderPrompt(): void {
    const promptInstr: TextRenderInstruction = new TextRenderInstruction(
      new Point(0, 32),
      new Point(this.context.clientSize.x, 40),
      MaterialColor.white,
      this.messageService.prompt,
      TextAlignment.center,
      32
    );
    promptInstr.pulseDuration = 6;
    this.instructions.push(promptInstr);
  }

  private renderActorHud(actor: PlayerActor): void {
    const numButtons: number = 10;

    const buttonsWidth: number = numButtons * 80;
    const buttonXStart: number = this.context.clientSize.x / 2 - numButtons / 2 * 80;
    const bottomBounds: number = this.context.clientSize.y - 80;

    // Add a rectangle for decoration
    const bgInstr: RectangleInstruction = new RectangleInstruction(
      new Point(buttonXStart - 2, bottomBounds - 2),
      new Point(buttonsWidth + 4, 80 + 4),
      MaterialColor.greyDark3
    );

    this.instructions.push(bgInstr);

    // Add buttons for the command bar area
    this.addHudCommandButtons(actor, buttonXStart, bottomBounds);

    // Figure out where to put the bars. If we're thin, they'll be on the top. Otherwise, they'll be on the sides
    const renderBarsOnSides: boolean = this.context.clientSize.x - buttonsWidth > 400;
    const barHeight: number = renderBarsOnSides ? 35 : 30;

    let stabilityPos: Point;
    let opsPos: Point;
    let barSize: Point;
    if (renderBarsOnSides) {
      barSize = new Point((this.context.clientSize.x - buttonsWidth) / 2 - 12, barHeight);
      stabilityPos = new Point(4, this.context.clientSize.y - 4 - barHeight);
      opsPos = new Point(this.context.clientSize.x - 4 - barSize.x, stabilityPos.y);
    } else {
      barSize = new Point(numButtons / 2 * 80, barHeight);
      stabilityPos = new Point(buttonXStart, bottomBounds - barHeight - 6);
      opsPos = new Point(this.context.clientSize.x / 2 + 3, stabilityPos.y);
    }

    this.addStabilityBarInstruction(stabilityPos, barSize);
    this.addOperationsBarInstruction(opsPos, barSize);
  }

  private addHudCommandButtons(actor: PlayerActor, buttonXStart: number, bottomBounds: number): void {
    let i: number = 0;
    for (const slot of actor.installedCommandSlots) {
      const buttonInstr: CommandButtonInstruction = new CommandButtonInstruction(new Point(buttonXStart + i * 80 + 2, bottomBounds + 3));

      let shortCutKey: string;
      if (i === 9) {
        shortCutKey = '0';
      } else {
        shortCutKey = String(i++);
      }

      this.addHudCommandButtonInstruction(slot, buttonInstr, shortCutKey);
    }
  }

  private addHudCommandButtonInstruction(slot: CommandSlot, buttonInstr: CommandButtonInstruction, displayIndex: string): void {
    const command: ActorCommand = slot.command;
    buttonInstr.slot = slot;

    if (command) {
      buttonInstr.id = `CommandButton_${displayIndex}`;
      buttonInstr.content = command.label;
      buttonInstr.clickFunction = (): boolean =>
        this.commandService.handleCommand(this.context.actor, GameCommand.internalCommand, command);
      buttonInstr.icon = command.icon;
      buttonInstr.iconOpacity = 0.15;

      if (this.gameLoopService.gameState === GameStatus.selectingTarget) {
        if (this.commandService.pendingCommand === command) {
          buttonInstr.isEnabled = true;
          buttonInstr.alwaysRenderHovered = true;
        } else {
          buttonInstr.isEnabled = false;
        }
      } else {
        buttonInstr.isEnabled = true;
      }

      buttonInstr.abilityCost = command.activationCostText;

      const tooltip: TooltipInfo = new TooltipInfo(command.name, command.description);
      buttonInstr.tooltip = tooltip;

      if (command instanceof ActiveCommand) {
        buttonInstr.isActive = command.isActive;
        if (command.isActive) {
          tooltip.footer = `Currently Active`;
        }
      } else {
        buttonInstr.isActive = null;
      }

      tooltip.rightHeaderText = buttonInstr.abilityCost;
    } else {
      buttonInstr.isEnabled = false;
      buttonInstr.content = null;
    }
    buttonInstr.hotkeyNumber = displayIndex;

    this.instructions.push(buttonInstr);
  }

  private addStabilityBarInstruction(position: Point, size: Point): void {
    const hpInstr: ProgressBarRenderInstruction = new ProgressBarRenderInstruction(
      position,
      size,
      MaterialColor.red,
      `Stability ${this.context.actor.currentHp} / ${this.context.actor.maxHp}`,
      this.context.actor.currentHp
    );
    hpInstr.maxValue = this.context.actor.maxHp;
    hpInstr.id = 'StabilityBar';

    hpInstr.tooltip = new TooltipInfo(
      hpInstr.label,
      `Stability indicates the health of a process. When this reaches 0 you will crash and the game will end. ` +
        `Some operations can restore damaged stability when executed.`
    );

    if (this.context.actor.currentHp <= this.context.actor.maxHp / 2) {
      hpInstr.pulseDuration = this.context.actor.currentHp / this.context.actor.maxHp * 4;
    }

    this.instructions.push(hpInstr);
  }

  private addOperationsBarInstruction(position: Point, size: Point): void {
    const opsInstruction: ProgressBarRenderInstruction = new ProgressBarRenderInstruction(
      position,
      size,
      MaterialColor.orange,
      `Operations ${this.context.actor.currentOps} / ${this.context.actor.maxOps}`,
      this.context.actor.currentOps
    );
    opsInstruction.alignRight = true;
    opsInstruction.maxValue = this.context.actor.maxOps;
    opsInstruction.id = 'OpsBar';

    const info: TooltipInfo = new TooltipInfo(
      opsInstruction.label,
      `Operations are computing power used perform new operations and to sustain active operations. Each turn you will recover 1 ` +
        `operation, but you will also need to power any active operations for them to remain active.`
    );

    info.footer = 'Regenerating +1/Turn';

    let perTurnCost: number = 0;

    const activeCommands: ActorCommand[] = this.context.actor.commands.filter(
      (c: ActorCommand): boolean => (<ActiveCommand>c).isActive === true
    );
    if (activeCommands.length > 0) {
      perTurnCost = activeCommands.map((c: ActiveCommand): number => c.activationCost).reduce((a: number, b: number): number => a + b);
    }

    if (perTurnCost > 0) {
      info.footer += `, Active Abilities: -${perTurnCost}/Turn`;
    }

    const delta: number = 1 - perTurnCost;
    info.rightHeaderText = delta > 0 ? `+${delta}/Turn` : (info.rightHeaderText = delta === 0 ? `No Change` : `${delta}/Turn`);

    opsInstruction.tooltip = info;

    this.instructions.push(opsInstruction);
  }

  private renderHeader(): void {
    this.instructions.push(
      new HeaderInstruction(
        new Point(0, 0),
        new Point(this.context.clientSize.x, this.headerHeight),
        MaterialColor.blueGreyDark4,
        'Emergence',
        environment.version
      )
    );
  }

  /**
   * Renders a collection of buttons
   */
  private renderButtons(): void {
    const buttonPadding: number = 12;
    let y: number = this.headerHeight + buttonPadding;

    for (const button of this.buttonService.getUiButtonData()) {
      this.instructions.push(this.buildUiButton(new Point(this.context.clientSize.x - buttonPadding - this.buttonSize.x, y), button));
      y += this.buttonSize.y + buttonPadding;
    }
  }
}
