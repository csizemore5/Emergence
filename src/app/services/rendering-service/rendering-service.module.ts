/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RenderInstructionGenerationService} from './render-instruction-generation.service';
import {RenderingService} from './rendering.service';

@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [RenderInstructionGenerationService, RenderingService]
})
export class RenderingServiceModule {}
