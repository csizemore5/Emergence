/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { EventEmitter, Injectable } from '@angular/core';
import * as PIXI from 'pixi.js';
import { PixiRenderingDirector } from '../../in-game/renderers/pixi/pixi-rendering-director';
import { SpriteReference } from '../../libraries/rendering/sprite-reference';
import { SpriteSheetReference } from '../../libraries/rendering/sprite-sheet-reference.enum';
import Rectangle = PIXI.Rectangle;
import Texture = PIXI.Texture;

@Injectable()
export class PixiTextureService {
  public texASCII: Texture[];
  public texSciFi: Texture[];
  public texSmall: Texture[];

  public loaded: EventEmitter<boolean>;
  public isLoaded: boolean = false;

  constructor() {
    this.loaded = new EventEmitter<boolean>();

    this.texASCII = [];
    this.texSciFi = [];
    this.texSmall = [];
  }

  private static getTextureForCharCode(charCode: number, sheet: SpriteSheetReference): Texture {
    const x: number = charCode % 16;
    const y: number = Math.floor(charCode / 16);

    let tileSize: number;
    let tex: Texture;
    switch (sheet) {
      case SpriteSheetReference.ascii:
        tex = PIXI.loader.resources.ascii.texture.clone();
        tileSize = PixiRenderingDirector.tileSize;
        break;

      case SpriteSheetReference.scifi:
        tex = PIXI.loader.resources.scifi.texture.clone();
        tileSize = 24;
        break;

      case SpriteSheetReference.small:
        tex = PIXI.loader.resources.small.texture.clone();
        tileSize = 16;
        break;

      default:
        throw new Error(`Unsupported sprite sheet reference ${sheet}`);
    }

    tex.frame = new Rectangle(x * tileSize, y * tileSize, tileSize, tileSize);

    return tex;
  }

  public load(): void {
    const loader: PIXI.loaders.Loader = PIXI.loader;
    loader.add('ascii', 'assets/ASCII.png');
    loader.add('scifi', 'assets/Sprites2.png');
    loader.add('small', 'assets/Sprites3.png');
    loader.load(this.onPIXILoad.bind(this));
  }

  public getSprite(spriteReference: SpriteReference): PIXI.Sprite {
    // Validate that this is something we think we can render
    const charCode: number = spriteReference.y * 16 + spriteReference.x;
    if (charCode < 0 || charCode > 255) {
      throw new Error(`Could not render character code ${charCode}`);
    }

    switch (spriteReference.sheet) {
      case SpriteSheetReference.ascii:
        return new PIXI.Sprite(this.texASCII[charCode]);

      case SpriteSheetReference.scifi:
        return new PIXI.Sprite(this.texSciFi[charCode]);

      case SpriteSheetReference.small:
        return new PIXI.Sprite(this.texSmall[charCode]);

      default:
        throw new Error(`Unknown sprite sheet reference ${spriteReference.sheet}`);
    }
  }

  private onPIXILoad(): void {
    // Set our textures
    for (let charCode: number = 0; charCode < 256; charCode++) {
      this.texASCII[charCode] = PixiTextureService.getTextureForCharCode(charCode, SpriteSheetReference.ascii);
      this.texSciFi[charCode] = PixiTextureService.getTextureForCharCode(charCode, SpriteSheetReference.scifi);
      this.texSmall[charCode] = PixiTextureService.getTextureForCharCode(charCode, SpriteSheetReference.small);
    }

    // Flag as loaded
    this.isLoaded = true;

    // Mark that we're loaded and check to see if we got instructions during the load process
    this.loaded.emit(true);
  }
}
