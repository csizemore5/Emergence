/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

export enum GameLevelType {
  characterSelectLevel,
  level1,
  level2,
  level3,
  level4,
  level5,
  bossLevel
}
