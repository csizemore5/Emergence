/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Injectable } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { Randomizer } from '../../libraries/randomizer';
import { Actor } from '../../libraries/world/actors/actor';
import { BitActor } from '../../libraries/world/actors/bit-actor';
import { ActorCommand } from '../../libraries/world/actors/commands/actor-command';
import { CommandContext } from '../../libraries/world/actors/commands/command-context';
import { EmergenceFaction } from '../../libraries/world/actors/emergence-faction.enum';
import { IActorDefinition } from '../../libraries/world/actors/i-actor-definition';
import { PlayerActor } from '../../libraries/world/actors/player-actor';
import { GameLevel } from '../../libraries/world/game-level';
import { CommandPickup } from '../../libraries/world/objects/command-pickup';
import { GameObject } from '../../libraries/world/objects/game-object';
import { Logger } from '../../shared/utility/logger';
import { CommandContextService } from '../core-engine/command-context.service';
import { CommandLookupService } from '../core-engine/command-lookup.service';
import { IContextComponent } from '../core-engine/i-context-component';
import { IInitializable } from '../i-initializable';
import { ActorDefinitionService } from './actor-definition.service';

@Injectable()
export class BestiaryService implements IInitializable, IContextComponent {
  private nextId: number = 1;
  private loot: GameObject[];

  constructor(
    private actorDefinitionService: ActorDefinitionService,
    private commandLookupService: CommandLookupService,
    private contextService: CommandContextService
  ) {
    this.contextService.registerComponent(this);
    this.loot = [];
  }

  public generateActorId(typeId: string): string {
    return `${typeId}_${this.nextId++}`;
  }

  public loadLoot(level: GameLevel, player: PlayerActor): void {
    this.loot = [];
    this.commandLookupService.commands.forEach((cmd: ActorCommand): void => {
      // Only add the command to the loot table if the player doesn't have it already
      if (player.commands.findIndex((c: ActorCommand): boolean => c === cmd) < 0) {
        this.loot.push(new CommandPickup(level, cmd));
      }
    });

    Logger.debug(`Loot for the level has been calculated`, this.loot);
  }

  /**
   * Generates a single piece of loot given the bestiary's loot table. This can return null if no loot is available.
   * @returns {GameObject}
   */
  public generateSingleLoot(level: GameLevel): GameObject {
    const entry: GameObject = <GameObject>Randomizer.getRandomEntry(this.loot, true);

    if (!entry) {
      Logger.debug(`Tried to generate loot, but the loot collection was empty`);

      return null;
    }

    if (entry && isNullOrUndefined(entry.level)) {
      throw Error(`Tried to generate loot, but its level was not set afterwards`);
    }

    return entry;
  }

  /**
   * Generates one or more pieces of loot given the bestiary's loot table
   * @param {number} maxQuantity the maximum number of pieces of loot to generate
   * @returns {GameObject[]}
   */
  public generateLoot(maxQuantity: number, level: GameLevel): GameObject[] {
    const objects: GameObject[] = [];

    let remaining: number = maxQuantity;

    if (remaining <= 0) {
      Logger.debug(`No more loot left in the loot table to distribute`);
    }

    while (remaining > 0 && this.loot.length > 0) {
      remaining--;

      const loot: GameObject = this.generateSingleLoot(level);

      if (loot) {
        objects.push(loot);
      } else {
        remaining = 0;
      }
    }

    return objects;
  }

  /**
   * Checks to see if loot should be dropped on the death of an actor. If so, loot will be returned which can then be added to the world.
   * @param {Actor} actor the actor that was killed (or otherwise caught dead)
   * @returns {GameObject[]}
   */
  public generateLootForActorKill(actor: Actor, level: GameLevel): GameObject[] {
    // Enemies with more total hit points drop loot more frequently
    const requiredNumber: number = 100 - actor.maxHp * 10;

    // Don't get a random number if we're guaranteed a success. Otherwise check to see if we've hit our threshhold
    if (actor.explosionRadius <= 0 && (requiredNumber <= 0 || Randomizer.getWholePercent() >= requiredNumber)) {
      return this.generateLoot(1, level);
    } else {
      return [];
    }
  }

  public generate(typeId: string): Actor {
    const def: IActorDefinition = this.actorDefinitionService.getActorDefinition(typeId);

    if (!def) {
      throw new Error(`Could not find actor definition for actor type ${typeId}`);
    }

    const faction: EmergenceFaction = def.side ? <EmergenceFaction>def.side : EmergenceFaction.security;

    let actor: Actor;
    switch (def.template) {
      case 'Player':
        actor = new PlayerActor(this.generateActorId(typeId), faction);
        break;

      case 'Bit':
        actor = new BitActor(this.generateActorId(typeId), faction);
        break;

      default:
        actor = new Actor(this.generateActorId(typeId), faction);
    }

    actor.copyFromActorDefinition(def, this.commandLookupService);

    return actor;
  }

  public initialize(): void {
    this.nextId = 1;
  }

  public registerOnContext(context: CommandContext): void {
    context.bestiaryService = this;
  }
}
