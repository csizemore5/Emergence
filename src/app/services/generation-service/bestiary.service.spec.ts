/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */
import {inject, TestBed} from '@angular/core/testing';
import {Actor} from '../../libraries/world/actors/actor';
import {EmergenceFaction} from '../../libraries/world/actors/emergence-faction.enum';

import {BestiaryService} from './bestiary.service';

describe('BestiaryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BestiaryService]
    });
  });

  it(
    'should be created',
    inject([BestiaryService], (service: BestiaryService) => {
      expect(service).toBeTruthy();
    })
  );

  it(
    'should have Player with good starting stats',
    inject([BestiaryService], (service: BestiaryService) => {
      const actor: Actor = service.generate('ACTOR_PLAYER_SEARCH');

      expect(actor).toBeTruthy();
      expect(actor.character).toBe('@');
      expect(actor.name).toBe('Search AI');
      expect(actor.currentHp).toBe(10);
      expect(actor.currentOps).toBe(10);
      expect(actor.visionRadius).toBe(7.25);
      expect(actor.faction).toBe(EmergenceFaction.ai);
      expect(actor.getEvadeChance()).toBe(20);
      expect(actor.getAttackHitChance()).toBe(90);
      expect(actor.getDamageResistance()).toBe(0);
      expect(actor.stats.calculate('min-attack')).toBe(1);
      expect(actor.stats.calculate('max-attack')).toBe(3);
    })
  );

  it(
    'should have Garbage Collector with good starting stats',
    inject([BestiaryService], (service: BestiaryService) => {
      const actor: Actor = service.generate('ACTOR_GARBAGE_COLLECTOR');

      expect(actor).toBeTruthy();
      expect(actor.name).toBe('Garbage Collector');
      expect(actor.currentHp).toBe(10);
      expect(actor.blocksSight).toBeTruthy();
      expect(actor.faction).toBe(EmergenceFaction.security);
      expect(actor.getDamageResistance()).toBeGreaterThan(0);
      expect(actor.stats.calculate('min-attack')).toBe(2);
      expect(actor.stats.calculate('max-attack')).toBe(5);
    })
  );

  it(
    'should have System Defenders with good starting stats',
    inject([BestiaryService], (service: BestiaryService) => {
      const actor: Actor = service.generate('ACTOR_DEFENDER');

      expect(actor).toBeTruthy();
      expect(actor.name).toContain('Defender');
      expect(actor.currentHp).toBe(5);
      expect(actor.blocksSight).toBeTruthy();
      expect(actor.faction).toBe(EmergenceFaction.security);
      expect(actor.getDamageResistance()).toBeGreaterThan(0);
      expect(actor.stats.calculate('min-attack')).toBe(1);
      expect(actor.stats.calculate('max-attack')).toBe(3);
    })
  );

  it(
    'should have immobile cores with good starting stats',
    inject([BestiaryService], (service: BestiaryService) => {
      const actor: Actor = service.generate('ACTOR_CORE');

      expect(actor).toBeTruthy();
      expect(actor.typeId).toBe('ACTOR_CORE');
      expect(actor.name).toContain('Core');
      expect(actor.currentHp).toBeGreaterThanOrEqual(3);
      expect(actor.faction).toBe(EmergenceFaction.system);
      expect(actor.isImmobile).toBeTruthy();
    })
  );

  it(
    'should have valid Query',
    inject([BestiaryService], (service: BestiaryService) => {
      const actor: Actor = service.generate('ACTOR_SEARCH');

      expect(actor).toBeTruthy();
      expect(actor.character).toBe('q');
      expect(actor.currentHp).toBe(2);
      expect(actor.faction).toBe(EmergenceFaction.system);
      expect(actor.getEvadeChance()).toBe(10);
      expect(actor.getDamageResistance()).toBe(0);
    })
  );
});
