/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

export enum FloorType {
  /// <summary>
  /// Represents nothingness. A convenience for cells that do not exist.
  /// </summary>
  Void = 0,
  /// <summary>
  /// A normal floor tile
  /// </summary>
  Normal = 1,
  /// <summary>
  /// A slightly differently decorated floor tile.
  /// </summary>
  DecorativeTile = 3,
  /// <summary>
  /// A decorative walkway.
  /// </summary>
  Walkway = 4,
  /// <summary>
  /// Indicates a caution marker
  /// </summary>
  CautionMarker = 5
}
