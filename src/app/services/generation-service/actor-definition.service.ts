/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Injectable } from '@angular/core';
import { IActorDefinition } from '../../libraries/world/actors/i-actor-definition';
import { Logger } from '../../shared/utility/logger';

@Injectable()
export class ActorDefinitionService {
  private readonly actorDefinitions: IActorDefinition[];

  constructor() {
    this.actorDefinitions = [];

    this.loadActorDefinitions();
  }

  public getActorDefinition(typeId: string): IActorDefinition {
    const lowerId: string = typeId.toLowerCase();

    return this.actorDefinitions.find((a: IActorDefinition): boolean => a.id.toLowerCase() === lowerId);
  }

  public getPlayerDefinitions(): IActorDefinition[] {
    Logger.debug(`Current Actor Definitions`, this.actorDefinitions);

    return this.actorDefinitions.filter((a: IActorDefinition): boolean => a.template === 'Player');
  }

  public getNonPlayerDefinitions(): IActorDefinition[] {
    return this.actorDefinitions.filter((a: IActorDefinition): boolean => a.template !== 'Player');
  }

  private loadActorDefinitions(): void {
    for (const def of require('json-loader!App/libraries/world/actors/bestiary.json')) {
      if (!def.accuracy) {
        def.accuracy = 85;
      }
      if (!def.evade) {
        def.evade = 10;
      }
      if (!def.opsRegen) {
        def.opsRegen = 1;
      }
      if (!def.minAttack) {
        def.minAttack = 1;
      }
      if (!def.maxAttack) {
        def.maxAttack = 1;
      }
      if (!def.damageResistance) {
        def.damageResistance = 0;
      }

      this.actorDefinitions.push(def);
    }
  }
}
