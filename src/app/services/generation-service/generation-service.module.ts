/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ActorDefinitionService } from './actor-definition.service';
import { BestiaryService } from './bestiary.service';
import { LevelGenerationService } from './level-generation.service';

@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [BestiaryService, LevelGenerationService, ActorDefinitionService]
})
export class GenerationServiceModule {}
