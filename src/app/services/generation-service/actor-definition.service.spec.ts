/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */
import {inject, TestBed} from '@angular/core/testing';

import {ActorDefinitionService} from './actor-definition.service';

describe('ActorDefinitionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActorDefinitionService]
    });
  });

  it(
    'should be created',
    inject([ActorDefinitionService], (service: ActorDefinitionService) => {
      expect(service).toBeTruthy();
    })
  );
});
