/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { ICellData } from './i-cell-data';

export interface ILevel {
  id: number;
  name: string;
  upperLeft: string;
  lowerRight: string;
  playerStart: string;
  cells: ICellData[];
}
