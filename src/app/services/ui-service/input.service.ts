/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Injectable } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { environment } from '../../../environments/environment';
import { Actor } from '../../libraries/world/actors/actor';
import { GameCommand } from '../../libraries/world/actors/commands/game-command.enum';
import { Point } from '../../libraries/world/point';
import { Logger } from '../../shared/utility/logger';
import { CommandService } from '../core-engine/command.service';
import { WorldService } from '../core-engine/world.service';
import { RenderingService } from '../rendering-service/rendering.service';
import { DialogService } from './dialog.service';
import { TooltipService } from './tooltip.service';

@Injectable()
export class InputService {
  private _keyCommandMap: any[];
  private _handlers: ((actor: Actor) => boolean)[];

  constructor(
    private dialogService: DialogService,
    private commandService: CommandService,
    private worldService: WorldService,
    private tooltipService: TooltipService,
    private renderingService: RenderingService
  ) {
    this._handlers = [];
    this._keyCommandMap = [];

    // Set up key bindings
    this.initializeKeyMappings();

    // Set up our knowledge of commands
    this.initializeCommandHandlers();
  }

  public handleKeypress(keyCode: number, character: string, isControlDown: boolean, isShiftDown: boolean): boolean {
    // In general, on keypress, remove the tooltip if one was present
    this.tooltipService.cancelTooltip();

    // Ignore all control events for now as those tend to go to the browser
    if (isControlDown) {
      return false;
    }

    let command: GameCommand = null;
    if (this._keyCommandMap[keyCode]) {
      command = this._keyCommandMap[keyCode];
    }
    Logger.debug(`Key press (key code: ${keyCode}, character: ${character}) interpreted as ${command}`);

    if (!isNullOrUndefined(command)) {
      const commandHandler: (actor: Actor) => boolean = this._handlers[command];
      if (!isNullOrUndefined(commandHandler)) {
        return commandHandler(this.worldService.player);
      }
    }

    return false;
  }

  private initializeCommandHandlers(): void {
    // Move handlers
    this.buildMoveCommand('W', -1, 0);
    this.buildMoveCommand('E', 1, 0);
    this.buildMoveCommand('N', 0, -1);
    this.buildMoveCommand('S', 0, 1);
    this.buildMoveCommand('NW', -1, -1);
    this.buildMoveCommand('NE', 1, -1);
    this.buildMoveCommand('SW', -1, 1);
    this.buildMoveCommand('SE', 1, 1);

    for (let i: number = 0; i <= 9; i++) {
      this._handlers[String(i)] = (): boolean => this.commandService.handleHotbarCommand(i === 0 ? 9 : i - 1);
    }

    // Other Commands
    this._handlers[GameCommand.cancel] = (): void => {
      this.dialogService.closeActiveDialog();
      this.commandService.cancelTargetedCommand();
    };
    this._handlers[GameCommand.wait] = (): boolean => this.commandService.handleWaitCommand(this.worldService.player);
    this._handlers[GameCommand.zoomIn] = (): void => this.renderingService.zoomIn();
    this._handlers[GameCommand.zoomOut] = (): void => this.renderingService.zoomOut();
    this._handlers[GameCommand.showInventory] = (): void => this.dialogService.showCharacterManagementScreen();
    // this._handlers['HELP'] = (): void => this.gameService.showHelpScreen();

    // Debug Commands
    if (environment.allowDebugMode) {
      this._handlers[GameCommand.toggleDebugMode] = (): boolean =>
        this.commandService.handleCommand(this.worldService.player, GameCommand.toggleDebugMode);
    }
  }

  private initializeKeyMappings(): void {
    this._keyCommandMap[12] = GameCommand.wait; // Numpad 5
    this._keyCommandMap[27] = GameCommand.cancel; // Escape
    this._keyCommandMap[32] = GameCommand.wait; // Spacebar
    this._keyCommandMap[37] = 'W'; // Numpad 4
    this._keyCommandMap[38] = 'N'; // Numpad 8
    this._keyCommandMap[39] = 'E'; // Numpad 6
    this._keyCommandMap[40] = 'S'; // Numpad 2
    this._keyCommandMap[48] = '0';
    this._keyCommandMap[49] = '1';
    this._keyCommandMap[50] = '2';
    this._keyCommandMap[51] = '3';
    this._keyCommandMap[52] = '4';
    this._keyCommandMap[53] = '5';
    this._keyCommandMap[54] = '6';
    this._keyCommandMap[55] = '7';
    this._keyCommandMap[56] = '8';
    this._keyCommandMap[57] = '9';
    this._keyCommandMap[65] = 'W'; // A
    this._keyCommandMap[68] = 'E'; // D
    this._keyCommandMap[73] = GameCommand.showInventory;
    this._keyCommandMap[83] = 'S'; // S
    this._keyCommandMap[87] = 'N'; // W
    this._keyCommandMap[98] = 'S'; // Numpad 2 with Numlock
    this._keyCommandMap[100] = 'W'; // Numpad 4 with Numlock
    this._keyCommandMap[101] = GameCommand.wait; // Numpad 5 with Numlock
    this._keyCommandMap[102] = 'E'; // Numpad 6 with Numlock
    this._keyCommandMap[104] = 'N'; // Numpad 8 with Numlock
    this._keyCommandMap[107] = GameCommand.zoomIn; // Num +
    this._keyCommandMap[109] = GameCommand.zoomOut; // Num -
    // this._keyCommandMap[112] = 'HELP'; // F1

    if (environment.allowDebugMode) {
      this._keyCommandMap[145] = GameCommand.toggleDebugMode; // Scroll Lock
    }

    this._keyCommandMap[189] = GameCommand.zoomOut; // -
    this._keyCommandMap[187] = GameCommand.zoomIn; // +
  }

  private buildMoveCommand(direction: any, xOffset: number, yOffset: number): void {
    this._handlers[direction] = (player: Actor): boolean =>
      this.commandService.handleCommand(player, GameCommand.move, player.pos.add(new Point(xOffset, yOffset)));
  }
}
