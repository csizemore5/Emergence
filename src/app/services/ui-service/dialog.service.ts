/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs/add/observable/of';
import { CommandContext } from '../../libraries/world/actors/commands/command-context';
import { Logger } from '../../shared/utility/logger';
import { CommandContextService } from '../core-engine/command-context.service';
import { GameLoopService } from '../core-engine/game-loop.service';
import { IContextComponent } from '../core-engine/i-context-component';

@Injectable()
export class DialogService implements IContextComponent {
  constructor(private gameLoopService: GameLoopService, private router: Router, private contextService: CommandContextService) {
    contextService.registerComponent(this);
  }

  /**
   * Closes the active dialog, if one is open.
   */
  public closeActiveDialog(): void {
    this.router.navigate([{ outlets: { dialog: null } }]);
  }

  /**
   * Shows the help screen
   */
  public showHelp(): void {
    this.router.navigate(['/help']);
  }

  /**
   * Shows the character management screen
   */
  public showCharacterManagementScreen(): void {
    Logger.debug(`Looking at the router`, this.router);
    if (this.router.url.includes('dialog')) {
      this.closeActiveDialog();
    } else {
      this.router.navigate([{ outlets: { dialog: ['character'] } }]);
    }
  }

  /**
   * Shows the in-game menu
   */
  public showMenu(isInGame: boolean): void {
    if (isInGame) {
      this.router.navigate([{ outlets: { dialog: ['menu'] } }]);
    } else {
      this.router.navigate(['/menu']);
    }
  }

  public registerOnContext(context: CommandContext): void {
    context.dialogService = this;
  }
}
