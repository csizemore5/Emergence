/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Injectable } from '@angular/core';
import { RenderInstruction } from '../../libraries/rendering/render-instruction';
import { Logger } from '../../shared/utility/logger';
import { RenderInstructionGenerationService } from '../rendering-service/render-instruction-generation.service';

@Injectable()
export class TooltipService {
  constructor(private rigService: RenderInstructionGenerationService) {}

  public requestTooltip(instruction: RenderInstruction): void {
    Logger.debug(`Requested tooltip`, instruction);
    this.rigService.requestTooltip(instruction);
  }

  public cancelTooltip(instruction: RenderInstruction = null): void {
    this.rigService.cancelTooltip(instruction);
  }
}
