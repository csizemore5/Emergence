/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {Injectable} from '@angular/core';
import {ButtonData} from '../../libraries/rendering/button-data';
import {SpriteReference} from '../../libraries/rendering/sprite-reference';
import {SpriteSheetReference} from '../../libraries/rendering/sprite-sheet-reference.enum';
import {TooltipInfo} from '../../libraries/rendering/tooltip-info';
import {DialogService} from './dialog.service';

@Injectable()
export class ButtonService {
  constructor(private dialogService: DialogService) {}

  /**
   * Gets a collection of buttons that should be displayed in the user interface
   * @returns {ButtonData[]}
   */
  public getUiButtonData(): ButtonData[] {
    const buttons: ButtonData[] = [];

    buttons.push(
      new ButtonData(
        new TooltipInfo('Menu', 'View the application menu.'),
        (): void => this.dialogService.showMenu(false),
        new SpriteReference(SpriteSheetReference.ascii, 4, 10),
        'settings'
      )
    );

    buttons.push(
      new ButtonData(
        new TooltipInfo('Help', 'Show helpful information about this game.'),
        (): void => this.dialogService.showHelp(),
        new SpriteReference(SpriteSheetReference.ascii, 15, 3),
        'help_outline'
      )
    );

    buttons.push(
      new ButtonData(
        new TooltipInfo('Inventory', 'View your inventory, select active commands, and change your attributes.'),
        (): void => this.dialogService.showCharacterManagementScreen(),
        new SpriteReference(SpriteSheetReference.ascii, 9, 6),
        'inbox'
      )
    );

    return buttons;
  }
}
