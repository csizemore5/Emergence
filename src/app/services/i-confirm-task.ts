/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

// import {EventEmitter} from '@angular/core';

export interface IConfirmTask {
  message: string;
  // key?: string;
  // icon?: string;
  header: string;
  accept(): void;
  reject(): void;
  // acceptVisible?: boolean;
  // rejectVisible?: boolean;
  // acceptEvent?: EventEmitter<any>;
  // rejectEvent?: EventEmitter<any>;
}
