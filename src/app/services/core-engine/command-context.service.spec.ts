/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {inject, TestBed} from '@angular/core/testing';

import {CommandContextService} from './command-context.service';

describe('CommandContextService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommandContextService]
    });
  });

  it('should be created', inject([CommandContextService], (service: CommandContextService) => {
    expect(service).toBeTruthy();
  }));
});
