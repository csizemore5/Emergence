/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CharacterClass } from '../../libraries/setup/character-class';
import { Actor } from '../../libraries/world/actors/actor';
import { ActiveCommand } from '../../libraries/world/actors/commands/active-command';
import { CommandContext } from '../../libraries/world/actors/commands/command-context';
import { CommandSlot } from '../../libraries/world/actors/commands/command-slot';
import { PlayerActor } from '../../libraries/world/actors/player-actor';
import { Cell } from '../../libraries/world/cell';
import { GameLevel } from '../../libraries/world/game-level';
import { GameObject } from '../../libraries/world/objects/game-object';
import { Point } from '../../libraries/world/point';
import { ArrayHelper } from '../../shared/utility/array-helper';
import { Logger } from '../../shared/utility/logger';
import { BestiaryService } from '../generation-service/bestiary.service';
import { GameLevelType } from '../generation-service/game-level-type.enum';
import { LevelGenerationService } from '../generation-service/level-generation.service';
import { IInitializable } from '../i-initializable';
import { MessageService } from '../ui-service/message.service';
import { CommandContextService } from './command-context.service';
import { IContextComponent } from './i-context-component';

@Injectable()
export class WorldService implements IInitializable, IContextComponent {
  private _player: PlayerActor;

  private _level: GameLevel;
  private _currentLevelType: GameLevelType;

  constructor(
    private levelGenerator: LevelGenerationService,
    private bestiaryService: BestiaryService,
    private contextService: CommandContextService,
    private messageService: MessageService
  ) {
    this.contextService.registerComponent(this);
  }

  public readonly levelLoaded: EventEmitter<GameLevel> = new EventEmitter<GameLevel>();

  public registerOnContext(context: CommandContext): void {
    context.worldService = this;
  }

  public advanceToNextLevel(): void {
    let nextLevelType: GameLevelType;

    switch (this.currentLevelType) {
      case GameLevelType.level1:
        nextLevelType = GameLevelType.level2;
        break;

      case GameLevelType.level2:
        nextLevelType = GameLevelType.level3;
        break;

      case GameLevelType.level3:
        nextLevelType = GameLevelType.level4;
        break;

      case GameLevelType.level4:
        nextLevelType = GameLevelType.level5;
        break;

      case GameLevelType.level5:
        nextLevelType = GameLevelType.bossLevel;
        break;

      default:
        nextLevelType = this.currentLevelType;
    }

    Logger.debug(`Advancing to level ${nextLevelType} from ${this._currentLevelType}`);
    this._currentLevelType = nextLevelType;

    // Ensure the admin access is gone before moving on
    this.player.hasAdminAccess = false;

    this.startLevel();
  }

  public get currentLevelType(): GameLevelType {
    return this._currentLevelType;
  }

  public get objects(): GameObject[] {
    return this._level.objects;
  }

  public get actors(): Actor[] {
    return this._level.actors;
  }

  public set currentLevel(value: GameLevel) {
    this._level = value;
  }

  public get currentLevel(): GameLevel {
    return this._level;
  }

  public initialize(): void {
    this._currentLevelType = GameLevelType.level1;
  }

  public generate(characterClass: CharacterClass): void {
    this.generatePlayer(characterClass);
    this.startLevel();
  }

  private startLevel(): void {
    this.generateLevel().subscribe((level: GameLevel) => {
      this.actors.push(this._player);
      this.setActorPos(this.player, level.playerStartPos);

      this.levelLoaded.emit(level);
    });
  }

  public generatePlayer(characterClass: CharacterClass): void {
    this._player = this.buildPlayer(characterClass);
    Logger.debug(`Built Player`, this._player);
  }

  private generateLevel(): Observable<GameLevel> {
    return this.levelGenerator.generateLevel(this, this._player, this.currentLevelType);
  }

  get player(): PlayerActor {
    return this._player;
  }

  public getCell(cellPos: Point): Cell {
    return this._level.getCell(cellPos);
  }

  public handleDeadActor(actor: Actor): void {
    // Ensure the actor is removed from the cell they were in
    const cell: Cell = this.getCell(actor.pos);

    // Kills generate loot - even if an actor killed itself somehow
    const loot: GameObject[] = actor.getDroppedLoot(this.currentLevel, this.bestiaryService);
    if (loot) {
      for (const item of loot) {
        if (!actor.isPlayer) {
          this.messageService.showMessage(`${actor.name} dropped ${item.name}.`);
        }
        this.currentLevel.addObject(item, cell.pos);
      }
    }

    if (cell && cell.actor === actor) {
      cell.actor = null;
    }
  }

  /**
   * Gets a 3x3 grid of cells with the requested position in the center of the grid
   * @param {Point} center
   * @param {boolean} includeCenter whether or not the center of the grid is included in the results
   * @param {boolean} includeDiagonal whether or not diagonal spots from the center of the grid will be included in the results
   * @param radius {number} The range + or - that will be drawn into the cell tile grid. By default this is 1, but this can be widened.
   * @returns {Cell[]}
   */
  public getCellGrid(center: Point, includeCenter: boolean = true, radius: number = 1, includeDiagonal: boolean = true): Cell[] {
    return this.currentLevel.getCellGrid(center, includeCenter, radius, includeDiagonal);
  }

  public setActorPos(actor: Actor, newPos: Point): void {
    if (actor === this.player) {
      Logger.debug(`Setting player position to ${newPos.toString()}`);
    }

    let cell: Cell = this.getCell(actor.pos);
    if (cell) {
      cell.actor = null;
    }

    actor.pos = newPos;

    cell = this.getCell(newPos);

    // No null check here. Going to assert that this is a good place since we're setting it
    cell.actor = actor;
  }

  public setObjectPos(object: GameObject, newPos: Point): void {
    let cell: Cell = this.getCell(object.pos);
    if (cell) {
      ArrayHelper.removeIfPresent(cell.objects, object);
    }

    object.pos = newPos;

    cell = this.getCell(newPos);
    cell.objects.push(object);
  }

  private buildPlayer(characterClass: CharacterClass): PlayerActor {
    const actor: PlayerActor = <PlayerActor>this.bestiaryService.generate(characterClass.playerClass);
    actor.isPlayer = true;

    actor.finalizeCommands();

    for (const slot of actor.installedCommandSlots.filter((c: CommandSlot): boolean => c.command && c.command instanceof ActiveCommand)) {
      actor.stats.registerModifier(<ActiveCommand>slot.command);
    }

    return actor;
  }
}
