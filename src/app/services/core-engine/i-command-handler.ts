/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {Actor} from '../../libraries/world/actors/actor';
import {GameCommand} from '../../libraries/world/actors/commands/game-command.enum';

export interface ICommandHandler {
  handleCommand(executingActor: Actor, commandType: GameCommand, data?: any): boolean;
}
