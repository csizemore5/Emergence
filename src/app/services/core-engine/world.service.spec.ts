/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */
import {inject, TestBed} from '@angular/core/testing';

import {WorldService} from './world.service';

describe('WorldService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorldService]
    });
  });

  it('should be created', inject([WorldService], (service: WorldService) => {
    expect(service).toBeTruthy();
  }));
});
