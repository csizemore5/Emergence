/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {inject, TestBed} from '@angular/core/testing';

import {CommandLookupService} from './command-lookup.service';

describe('CommandLookupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommandLookupService]
    });
  });

  it('should be created', inject([CommandLookupService], (service: CommandLookupService) => {
    expect(service).toBeTruthy();
  }));
});
