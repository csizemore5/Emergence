/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {AiServiceModule} from '../ai-service/ai-service.module';
import {GenerationServiceModule} from '../generation-service/generation-service.module';
import {CombatService} from './combat.service';
import {CommandContextService} from './command-context.service';
import {CommandLookupService} from './command-lookup.service';
import {CommandService} from './command.service';
import {GameLoopService} from './game-loop.service';
import {WorldService} from './world.service';

@NgModule({
  imports: [CommonModule, AiServiceModule, GenerationServiceModule],
  declarations: [],
  providers: [CombatService, CommandService, CommandContextService, GameLoopService, WorldService, CommandLookupService]
})
export class CoreEngineModule {}
