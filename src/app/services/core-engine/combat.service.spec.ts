import {inject, TestBed} from '@angular/core/testing';

import {CombatService} from './combat.service';

describe('CombatService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CombatService]
    });
  });

  it('should be created', inject([CombatService], (service: CombatService) => {
    expect(service).toBeTruthy();
  }));
});
