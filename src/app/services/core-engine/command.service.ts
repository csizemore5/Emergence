/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Injectable } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { environment } from '../../../environments/environment';
import { GameStatus } from '../../libraries/game-status';
import { Actor } from '../../libraries/world/actors/actor';
import { ActiveCommand } from '../../libraries/world/actors/commands/active-command';
import { ActorCommand } from '../../libraries/world/actors/commands/actor-command';
import { CommandContext } from '../../libraries/world/actors/commands/command-context';
import { GameCommand } from '../../libraries/world/actors/commands/game-command.enum';
import { TargetedCommand } from '../../libraries/world/actors/commands/targeted-command';
import { PlayerActor } from '../../libraries/world/actors/player-actor';
import { Cell } from '../../libraries/world/cell';
import { Point } from '../../libraries/world/point';
import { Logger } from '../../shared/utility/logger';
import { BestiaryService } from '../generation-service/bestiary.service';
import { IInitializable } from '../i-initializable';
import { RenderingService } from '../rendering-service/rendering.service';
import { MessageService } from '../ui-service/message.service';
import { CombatService } from './combat.service';
import { CommandContextService } from './command-context.service';
import { GameLoopService } from './game-loop.service';
import { ICommandHandler } from './i-command-handler';
import { IContextComponent } from './i-context-component';
import { WorldService } from './world.service';

@Injectable()
export class CommandService implements ICommandHandler, IInitializable, IContextComponent {
  private _pendingCommand: TargetedCommand;

  constructor(
    private contextService: CommandContextService,
    private worldService: WorldService,
    private bestiaryService: BestiaryService,
    private combatService: CombatService,
    private messageService: MessageService,
    private renderingService: RenderingService,
    private gameLoopService: GameLoopService
  ) {
    this.contextService.registerComponent(this);
  }

  get pendingCommand(): TargetedCommand {
    return this._pendingCommand;
  }

  public initialize(): void {
    this._pendingCommand = null;
  }

  public handleCommand(executingActor: Actor, commandType: GameCommand, data: any = null): boolean {
    // Allow the user to cancel a command
    if (this.gameLoopService.gameState === GameStatus.selectingTarget) {
      this.cancelTargetedCommand();

      return false;
    }

    if (this.gameLoopService.gameState !== GameStatus.standard) {
      Logger.warn(`Ignoring command since current state is ${this.gameLoopService.gameState}`);

      return false;
    }

    if (!this.gameLoopService.isProcessingCommand && executingActor.isPlayer) {
      this.gameLoopService.beforePlayerCommand();
    }

    switch (commandType) {
      case GameCommand.wait:
        return this.handleWaitCommand(executingActor);

      case GameCommand.move:
        return this.handleCellCommand(executingActor, data);

      case GameCommand.internalCommand:
        return this.handleActorCommand(executingActor, data);

      case GameCommand.toggleDebugMode:
        if (environment.allowDebugMode) {
          this.gameLoopService.isDebugMode = !this.gameLoopService.isDebugMode;
          this.renderingService.requestRender(true);

          return true;
        } else {
          return false;
        }

      default:
      // No operation needed
    }

    return false;
  }

  public requestTargetForCommand(command: TargetedCommand, targetMessage: string): void {
    this._pendingCommand = command;

    this.gameLoopService.gameState = GameStatus.selectingTarget;
    this.messageService.prompt = targetMessage;
    this.renderingService.requestRender(false);
  }

  public specifyTargetForCommand(cell: Cell): boolean {
    if (!this._pendingCommand) {
      throw new Error('Tried to handle targeted command but command was not known.');
    }

    this._pendingCommand.target = cell;

    const checkResult: string = this._pendingCommand.checkValidTarget(cell);
    if (checkResult) {
      this.messageService.showMessage(checkResult);
      this.renderingService.requestRender(false);

      return false;
    } else {
      this.gameLoopService.gameState = GameStatus.standard;
      this.handleQualifiedActorCommand(this._pendingCommand, this._pendingCommand.executionContext.invoker);
    }

    // Clear out the state
    this.cancelTargetedCommand();

    return true;
  }

  public cancelTargetedCommand(): void {
    this._pendingCommand = null;
    this.gameLoopService.gameState = GameStatus.standard;
    this.messageService.prompt = '';
    this.renderingService.requestRender(false);
  }

  public handleWaitCommand(actor: Actor): boolean {
    if (actor.isPlayer) {
      this.gameLoopService.isProcessingCommand = true;

      this.messageService.showMessage('Time passes...');
      this.advanceGameTurn();

      this.gameLoopService.isProcessingCommand = false;
    }

    return true;
  }

  public handleHotbarCommand(commandIndex: number): boolean {
    const command: ActorCommand = this.worldService.player.getCommand(commandIndex);
    if (!isNullOrUndefined(command)) {
      return this.handleCommand(this.worldService.player, GameCommand.internalCommand, command);
    }

    return false;
  }

  public registerOnContext(context: CommandContext): void {
    context.commandService = this;
  }

  private handleActorCommand(actor: Actor, command: ActorCommand): boolean {
    if (this.gameLoopService.gameState === GameStatus.selectingTarget) {
      this.cancelTargetedCommand();

      return false;
    }

    // Ensure things are handed off properly
    if (actor.isPlayer) {
      if (!this.gameLoopService.isProcessingCommand) {
        this.gameLoopService.beforePlayerCommand();
      } else {
        throw new Error('Already processing a player command');
      }
    }

    if (command instanceof TargetedCommand) {
      Logger.debug(`Handling Targeted Command`);

      const context: CommandContext = this.contextService.buildCommandContext(actor);
      command.executionContext = context;
      this._pendingCommand = command;
      this.requestTargetForCommand(command, command.getTargetMessage(context));

      return false;
    }

    return this.handleQualifiedActorCommand(command, actor);
  }

  private handleCellCommand(actor: Actor, cellPos: Point): boolean {
    // Do not allow commands on cells other than the adjacent ones
    if (!actor.pos.isAdjacentTo(cellPos, false) && !actor.pos.equals(cellPos)) {
      return false;
    }

    // Special validation for input from the player
    if (actor.isPlayer) {
      // If we got spammed with commands, ignore the latest one
      if (this.gameLoopService.isProcessingCommand) {
        this.messageService.showMessage('The game is still responding to your previous move. Please wait...');

        return false;
      }

      // Indicate that we're not ready to receive input yet
      this.gameLoopService.isProcessingCommand = true;
    }

    // If we're cell commanding on the cell we're currently occupying, treat it as a wait
    if (cellPos.equals(actor.pos)) {
      return this.handleWaitCommand(actor);
    }

    const cell: Cell = this.worldService.getCell(cellPos);

    if (cell.actor) {
      // Route this into an attack command
      this.combatService.handleAttack(this.contextService.buildCommandContext(actor), cell.actor, false);
    } else if (cell.hasInteractiveObjects && actor.isPlayer) {
      this.handleInteractiveCell(<PlayerActor>actor, cell);
    } else {
      // Treat it as a move
      this.handleMoveCommand(actor, cell);
    }

    if (actor.isPlayer) {
      this.advanceGameTurn();
    }

    return true;
  }

  private handleQualifiedActorCommand(command: ActorCommand, actor: Actor): boolean {
    Logger.debug(`Handling qualified actor command`, command, actor);

    if (!this.gameLoopService.isInGame) {
      Logger.warn(`Cannot execute command since current game state is ${this.gameLoopService.gameState}`);

      return false;
    }

    const insufficientOps: boolean = command.activationCost > actor.currentOps;
    const isActiveCommand: boolean = !(command instanceof ActiveCommand) && (<ActiveCommand>command).isActive;

    if (insufficientOps && !this.gameLoopService.isDebugMode && isActiveCommand) {
      // We'll treat this as their turn. This should never be the player's action and it'd be cool to "mana drain" an enemy
      this.messageService.showMessage(`${actor.name} tries to run ${command.name} but doesn't have enough enough operations available.`);
    } else {
      const invoked: boolean = command.invoke(this.contextService.buildCommandContext(actor));

      // Infinite operations on debug mode
      if (invoked && !this.gameLoopService.isDebugMode) {
        actor.changeOpsPoints(-command.activationCost);
      }
    }

    // If this was the player's action, make time go on
    if (actor.isPlayer) {
      this.advanceGameTurn();
    }

    return true;
  }

  private advanceGameTurn(): void {
    this.gameLoopService.incrementTurnCount();
    this.gameLoopService.processGameLoop(this);
  }

  private handleMoveCommand(actor: Actor, cell: Cell): void {
    if (!cell.isImpassible || (actor.isPlayer && this.gameLoopService.isDebugMode)) {
      this.worldService.setActorPos(actor, cell.pos);
    } else {
      if (actor.isPlayer) {
        this.messageService.showMessage(`You can't go that way.`);
      } else {
        Logger.debug(`${actor.name} tries to walk through something impassible.`);
      }
    }
  }

  private handleInteractiveCell(actor: PlayerActor, cell: Cell): void {
    // By default, the move will succeed
    let blocked: boolean = false;

    // Give objects a chance to object
    for (const obj of cell.objects) {
      if (obj.handleInteraction(actor, this.contextService.buildCommandContext(actor))) {
        blocked = true;
      }
    }

    // Okay, we'll allow the move
    if (!blocked) {
      this.handleMoveCommand(actor, cell);
    }
  }
}
