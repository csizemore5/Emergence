/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {Component, OnInit} from '@angular/core';
import {DropEvent} from 'ng-drag-drop';
import {ActorCommand} from '../../libraries/world/actors/commands/actor-command';
import {CommandSlot} from '../../libraries/world/actors/commands/command-slot';
import {PlayerActor} from '../../libraries/world/actors/player-actor';
import {WorldService} from '../../services/core-engine/world.service';
import {DragonDropHelper} from '../../shared/utility/dragon-drop-helper';
import {Logger} from '../../shared/utility/logger';

@Component({
  selector: 'em-command-management-screen',
  templateUrl: './command-management-screen.component.html',
  styleUrls: ['./command-management-screen.component.scss']
})
export class CommandManagementScreenComponent implements OnInit {
  /**
   * The actor to represent in the dialog.
   */
  public actor: PlayerActor;

  /**
   * The currently selected command
   * @type {ActorCommand}
   */
  public get selectedCommand(): ActorCommand {
    return this.selectedSlot ? this.selectedSlot.command : null;
  }

  /**
   * The currently selected command slot
   * @type {CommandSlot}
   */
  public selectedSlot: CommandSlot = null;

  constructor(private worldService: WorldService) {}

  /**
   * Reacts to a dragon drop event
   * @param {CommandSlot} target the slot that was dragged onto
   * @param {DropEvent} e the event data containing information on the card that was dragged.
   */
  public onCommandDropped(target: CommandSlot, e: DropEvent): void {
    Logger.debug(`Command drop detected`, target, e);

    const source: CommandSlot = <CommandSlot>e.dragData;

    DragonDropHelper.handleCommandDragDrop(target, source);

    // Ensure selection is swapped too
    if (this.selectedSlot === source) {
      this.selectedSlot = target;
    }
  }

  public selectSlot(slot: CommandSlot): void {
    this.selectedSlot = slot;
  }

  /**
   * `ngOnInit` is called right after the directive's data-bound properties have been checked for the
   * first time, and before any of its children have been checked. It is invoked only once when the
   * directive is instantiated.
   */
  public ngOnInit(): void {
    this.actor = this.worldService.player;
  }
}
