/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {NgDragDropModule} from 'ng-drag-drop';
import {SharedModule} from '../shared/shared.module';
import {CharacterManagementComponent} from './character-management/character-management.component';
import {CommandManagementScreenComponent} from './command-management-screen/command-management-screen.component';

@NgModule({
  imports: [CommonModule, SharedModule, NgDragDropModule.forRoot()],
  declarations: [CommandManagementScreenComponent, CharacterManagementComponent]
})
export class CharacterManagementModule {}
