/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GameTestingModule } from '../../game-testing/game-testing.module';

import { CharacterManagementComponent } from './character-management.component';

describe('CharacterManagementComponent', () => {
  let component: CharacterManagementComponent;
  let fixture: ComponentFixture<CharacterManagementComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [GameTestingModule]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
