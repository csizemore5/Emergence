/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {Cell} from '../world/cell';
import {TooltipInfo} from './tooltip-info';

export class CellColorizer {
  public getCellTooltip(cell: Cell, isDebugMode: boolean, isVisible: boolean): TooltipInfo {
    const info: TooltipInfo = new TooltipInfo('Cell Information', '');

    if (cell.actor && isVisible) {
      info.body = `\r\n${cell.actor.name} (${cell.actor.currentHp} / ${cell.actor.maxHp} Stability)`;
    }

    for (const obj of cell.objects) {
      info.body += `\r\n${obj.name}`;
    }

    if (info.body.length <= 0) {
      info.body = `\r\nUnallocated Space`;
    }

    if (isDebugMode) {
      info.title += ` ${cell.pos.toString()}`;
      if (cell.actor) {
        info.footer = `${cell.actor.id}`;
      }
    }

    info.renderThumbnail = true;

    return info;
  }
}
