/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

export class TooltipInfo {
  public title: string;
  public body: string;
  public footer: string;
  public rightHeaderText: string;
  public renderThumbnail: boolean;

  constructor(title: string, body: string) {
    this.title = title;
    this.body = body;
  }
}
