/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {SpriteSheetReference} from './sprite-sheet-reference.enum';

export class SpriteReference {
  public sheet: SpriteSheetReference;
  public x: number;
  public y: number;
  public tint: string;
  public opacity: number;

  constructor(sheet: SpriteSheetReference, x: number, y: number, tint: string = '', opacity: number = 1) {
    this.sheet = sheet;
    this.x = x;
    this.y = y;
    this.tint = tint;
    this.opacity = opacity;
  }
}
