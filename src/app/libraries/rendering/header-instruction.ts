/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {Point} from '../world/point';
import {RenderInstruction} from './render-instruction';
import {RenderingLayer} from './rendering-layer.enum';

export class HeaderInstruction extends RenderInstruction {
  public name: string;
  public version: string;

  constructor(screenPos: Point, size: Point, color: string, name: string, version: string) {
    super(screenPos, size, color);

    this.name = name;
    this.version = version;
    this.renderingLayer = RenderingLayer.header;
  }
}
