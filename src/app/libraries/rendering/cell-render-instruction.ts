/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { MaterialColor } from '../material-color.enum';
import { Cell } from '../world/cell';
import { ICellInstructionGenerationContext } from './generation/i-cell-instruction-generation-context';
import { RenderInstruction } from './render-instruction';
import { RenderingLayer } from './rendering-layer.enum';

export class CellRenderInstruction extends RenderInstruction {
  public context: ICellInstructionGenerationContext;
  public cell: Cell;
  public cursorColor: MaterialColor;

  constructor(context: ICellInstructionGenerationContext, cell: Cell) {
    super(context.screenPos, context.charSize);

    this.context = context;
    this.cell = cell;
    this.id = `Cell_${cell.pos.toString()}`;
    this.renderingLayer = RenderingLayer.level;
  }
}
