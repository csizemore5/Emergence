/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {MaterialColor} from '../material-color.enum';
import {Point} from '../world/point';
import {RenderInstruction} from './render-instruction';
import {RenderingLayer} from './rendering-layer.enum';

export class ProgressBarRenderInstruction extends RenderInstruction {

  public label: string;
  public value: number;
  public minValue: number = 0;
  public maxValue: number = 0;
  public foreColor: string = MaterialColor.white;
  public alignRight: boolean = false;

  constructor(screenPos: Point, size: Point, color: string, label: string = '', value: number = 0) {
    super(screenPos, size, color);

    this.label = label;
    this.value = value;
    this.renderingLayer = RenderingLayer.commandBar;

  }

}
