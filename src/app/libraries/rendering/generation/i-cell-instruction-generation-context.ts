/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {Actor} from '../../world/actors/actor';
import {Point} from '../../world/point';
import {CellColorizer} from '../cell-colorizer';

export interface ICellInstructionGenerationContext {
  colorizer: CellColorizer;
  isDebugMode: boolean;
  isInitialRender: boolean;
  isVisible: boolean;
  isKnown: boolean;
  actor: Actor;
  cellPos: Point;
  screenPos: Point;
  charSize: Point;
}
