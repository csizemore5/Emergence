/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {PlayerActor} from '../../world/actors/player-actor';
import {Point} from '../../world/point';

export interface IGenerationContext {
  actor: PlayerActor;
  isInitialRender: boolean;
  cellOffset: Point;
  maxColumns: number;
  maxRows: number;
  charSize: Point;
  clientSize: Point;
}
