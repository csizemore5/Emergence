/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { RenderInstruction } from './render-instruction';

export interface IRenderingDirector {
  handleWidthInvalidated(width: number, height: number, zoomLevel: number): void;

  render(instructions: RenderInstruction[]): void;
}
