/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {Point} from '../world/point';
import {RenderInstruction} from './render-instruction';
import {RenderingLayer} from './rendering-layer.enum';
import {TextAlignment} from './text-alignment';

export class TextRenderInstruction extends RenderInstruction {
  public text: string;
  public alignment: TextAlignment;
  public fontSize: number;

  constructor(
    screenPos: Point,
    size: Point,
    color: string,
    text: string,
    alignment: TextAlignment = TextAlignment.left,
    fontSize: number = 18
  ) {
    super(screenPos, size, color);

    this.text = text;
    this.alignment = alignment;
    this.fontSize = fontSize;
    this.renderingLayer = RenderingLayer.prompt;
  }
}
