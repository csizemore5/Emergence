/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {MaterialColor} from '../material-color.enum';
import {Point} from '../world/point';
import {ButtonInstruction} from './button-instruction';
import {RenderingLayer} from './rendering-layer.enum';

export class CommandButtonInstruction extends ButtonInstruction {
  public hotkeyNumber: string = '?';
  public abilityCost: string = null;
  public costColor: string = MaterialColor.orange;
  public isActive: boolean = null;

  constructor(screenPos: Point) {
    super(screenPos, new Point(75, 75), MaterialColor.blueGreyDark4);
    this.renderingLayer = RenderingLayer.commandBar;
    this.hoverColor = MaterialColor.teal;
  }
}
