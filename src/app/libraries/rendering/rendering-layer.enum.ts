/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

export enum RenderingLayer {
  systemButtons = 'sysButtons',
  level = 'level',
  commandBar = 'commandBar',
  header = 'header',
  prompt = 'prompt',
  log = 'log',
  tooltip = 'tooltip'
}
