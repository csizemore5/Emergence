/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { ActorCommand } from '../world/actors/commands/actor-command';
import { CharacterClassIconType } from './character-class-icon-type.enum';

/**
 * Represents a character class during game setup.
 */
export class CharacterClass {
  public name: string;
  public shortName: string;

  public iconType: CharacterClassIconType;

  public iconName: string;

  public initialCommands: ActorCommand[];

  public isDebugger: boolean = false;

  public playerClass: string;

  constructor(name: string, shortName: string, playerClass: string) {
    this.name = name;
    this.shortName = shortName;
    this.playerClass = playerClass;
    this.iconType = CharacterClassIconType.materialIcon;
    this.iconName = 'play_arrow';
    this.initialCommands = [];
  }
}
