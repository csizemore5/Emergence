export enum CharacterClassIconType {
  fontAwesome = 'fontAwesome',
  materialIcon = 'materialIcon'
}
