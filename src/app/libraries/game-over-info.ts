/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

export class GameOverInfo {
  public isVictory: boolean;
  public turnsTaken: number = 0;
  public numKills: number = 0;
  public damageDealt: number = 0;
  public damageReceived: number = 0;
  public score: number = 0;

  public calculateScore(): number {
    if (this.turnsTaken < 1) {
      this.turnsTaken = 1;
    }

    // Give credit for doing more damage than you received
    this.score = (this.damageDealt - this.damageReceived) * 5;

    // Give credit per kill, but try to encourage the user to go quickly
    this.score += Math.round(this.numKills / (this.turnsTaken * 100));

    // Also give credit for staying alive
    this.score += this.turnsTaken;

    // Winning is bonus points for sure
    if (this.isVictory) {
      this.score += 10000;
    }

    return this.score;
  }
}
