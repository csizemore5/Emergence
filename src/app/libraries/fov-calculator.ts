/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import * as TheField from 'the-field';
import { FieldOfViewMap, MaskRect } from 'the-field';
import { Actor } from './world/actors/actor';
import { Cell } from './world/cell';
import { GameLevel } from './world/game-level';
import { Point } from './world/point';

export class FovCalculator {
  public static calculateVisibleCellsForActor(actor: Actor, level: GameLevel): Cell[] {
    if (!level) {
      throw new Error(`Level is required to calculate visible cells`);
    }

    const visionBoost: number = actor.stats.calculate('vision-boost');
    actor.visibleCells = this.calculateVisibleCellsFromPoint(actor.pos, actor.visionRadius + visionBoost, level);

    return actor.visibleCells;
  }

  public static calculateVisibleCellsFromPoint(pos: Point, visionRadius: number, level: GameLevel): Cell[] {
    const offset: Point = new Point(pos.x - Math.floor(visionRadius), pos.y - Math.floor(visionRadius));

    const candidates: Cell[] = level.getCellGrid(pos, true, Math.floor(visionRadius));
    const fov: MaskRect = FovCalculator.getFieldOfView(pos, visionRadius, candidates, offset);

    const cells: Cell[] = [];
    for (const cell of candidates) {
      const distance: number = pos.calculateDistanceFrom(cell.pos);
      if (distance <= visionRadius) {
        if (cell && fov.get(cell.pos.x - offset.x, cell.pos.y - offset.y)) {
          cells.push(cell);
        }
      }
    }

    return cells;
  }

  private static getFieldOfView(pos: Point, visionRadius: number, candidates: Cell[], offset: Point): MaskRect {
    const fovRange: number = Math.floor(visionRadius * 2 + 1);
    const map: FieldOfViewMap = new TheField.FieldOfViewMap(fovRange, fovRange);

    for (const cell of candidates) {
      if (cell && !cell.pos.equals(pos) && cell.generatesSightBlocker) {
        map.addBody(cell.pos.x - offset.x, cell.pos.y - offset.y);
      }
    }

    return map.getFieldOfView(pos.x - offset.x, pos.y - offset.y, Math.floor(visionRadius));
  }
}
