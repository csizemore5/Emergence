/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { MaterialColor } from '../../material-color.enum';
import { SpriteReference } from '../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../rendering/sprite-sheet-reference.enum';
import { Actor } from '../actors/actor';
import { CommandContext } from '../actors/commands/command-context';
import { GameLevel } from '../game-level';
import { GameObject } from './game-object';

export class Metadata extends GameObject {
  constructor(level: GameLevel) {
    super(level);
  }

  get name(): string {
    return 'Metadata';
  }

  get spriteReference(): SpriteReference {
    return new SpriteReference(SpriteSheetReference.small, 0, 0);
  }

  get foregroundColor(): string {
    return MaterialColor.grey;
  }

  get blocksSight(): boolean {
    return false;
  }

  get blocksMovement(): boolean {
    return false;
  }

  get backgroundColor(): string {
    return undefined;
  }

  public handleInteraction(actor: Actor, context: CommandContext): boolean {
    return false;
  }

  public allowAiMovement(actor: Actor): boolean {
    return true;
  }
}
