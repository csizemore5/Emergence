/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { SpriteReference } from '../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../rendering/sprite-sheet-reference.enum';
import { ActorCommand } from '../actors/commands/actor-command';
import { CommandClassificationType } from '../actors/commands/command-classification-type.enum';
import { CommandContext } from '../actors/commands/command-context';
import { CommandSlot } from '../actors/commands/command-slot';
import { PlayerActor } from '../actors/player-actor';
import { GameLevel } from '../game-level';
import { Pickup } from './pickup';

/**
 * Represents a command object that can be picked up and installed by the player (or potentially other actors).
 */
export class CommandPickup extends Pickup {
  public command: ActorCommand;

  constructor(level: GameLevel, command: ActorCommand) {
    super(level);

    this.command = command;
  }

  get name(): string {
    return `${this.command.name} Command`;
  }

  get spriteReference(): SpriteReference {
    switch (this.command.commandType) {
      case CommandClassificationType.offensive:
        return new SpriteReference(SpriteSheetReference.small, 6, 4);

      case CommandClassificationType.restorative:
      case CommandClassificationType.defensive:
        return new SpriteReference(SpriteSheetReference.small, 5, 4);

      case CommandClassificationType.movement:
        return new SpriteReference(SpriteSheetReference.small, 3, 4);

      default:
        return new SpriteReference(SpriteSheetReference.small, 4, 4);
    }
  }

  public handleInteraction(actor: PlayerActor, context: CommandContext): boolean {
    const vacantSlot: CommandSlot = actor.findEmptyCommandSlot();
    if (vacantSlot) {
      // Give the object to the player and log the event
      vacantSlot.command = this.command;
      context.output(`${actor.name} picks up ${this.name}.`);

      // Now we need to destroy this object and ensure it no longer occurs in the wild
      this.level.removeObject(this);

      return true;
    } else {
      context.output(`Can't pick up ${this.name}; no vacant slot!`);

      return false;
    }
  }
}
