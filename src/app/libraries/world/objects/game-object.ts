/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { isNullOrUndefined } from 'util';
import { SpriteReference } from '../../rendering/sprite-reference';
import { Actor } from '../actors/actor';
import { CommandContext } from '../actors/commands/command-context';
import { Cell } from '../cell';
import { GameLevel } from '../game-level';
import { Point } from '../point';

export abstract class GameObject {
  public level: GameLevel;
  public pos: Point;

  protected constructor(level: GameLevel) {
    // Validate input
    if (isNullOrUndefined(level)) {
      throw Error(`Tried to create a ${this.constructor.name} without a level`);
    }

    this.level = level;
  }

  abstract get name(): string;

  abstract get spriteReference(): SpriteReference;

  abstract get foregroundColor(): string;
  abstract get blocksSight(): boolean;
  abstract get blocksMovement(): boolean;

  get pulseDuration(): number {
    return undefined;
  }

  get zIndex(): number {
    return 10;
  }

  get cell(): Cell {
    return this.level.getCell(this.pos);
  }

  get backgroundColor(): string {
    return undefined;
  }

  public handleInteraction(actor: Actor, context: CommandContext): boolean {
    context.output(`${actor.name} interacts with ${this.name}`);

    return false;
  }

  public allowAiMovement(actor: Actor): boolean {
    return !this.blocksMovement;
  }
}
