/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {Cell} from '../cell';
import {GameLevel} from '../game-level';
import {Point} from '../point';
import {GameObject} from './game-object';

export class BitwiseObjectHelper {
  public static calculateBits(obj: GameObject, checkFunc: (o: GameObject) => boolean): number {
    const leftPresent: boolean = this.checkIfPresent(obj.pos.offset(-1, 0), obj.level, checkFunc);
    const topPresent: boolean = this.checkIfPresent(obj.pos.offset(0, -1), obj.level, checkFunc);
    const rightPresent: boolean = this.checkIfPresent(obj.pos.offset(1, 0), obj.level, checkFunc);
    const bottomPresent: boolean = this.checkIfPresent(obj.pos.offset(0, 1), obj.level, checkFunc);

    let bitTotal: number = 0;
    if (topPresent) {
      bitTotal += 1;
    }
    if (rightPresent) {
      bitTotal += 2;
    }
    if (bottomPresent) {
      bitTotal += 4;
    }
    if (leftPresent) {
      bitTotal += 8;
    }

    return bitTotal;
  }

  private static checkIfPresent(pos: Point, level: GameLevel, checkFunc: (o: GameObject) => boolean): boolean {
    const cell: Cell = level.getCell(pos);

    return cell && cell.objects.filter(checkFunc).length > 0;
  }
}
