/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { MaterialColor } from '../../material-color.enum';
import { SpriteReference } from '../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../rendering/sprite-sheet-reference.enum';
import { Actor } from '../actors/actor';
import { CommandContext } from '../actors/commands/command-context';
import { GameLevel } from '../game-level';
import { DestructibleObject } from './destructible-object';

export class Divider extends DestructibleObject {
  constructor(level: GameLevel) {
    super(level, 2);
  }

  get name(): string {
    return 'Viewport';
  }

  get spriteReference(): SpriteReference {
    return new SpriteReference(SpriteSheetReference.small, 1, 7);
  }

  get foregroundColor(): string {
    return MaterialColor.brownLight1;
  }

  get blocksSight(): boolean {
    return false;
  }

  get blocksMovement(): boolean {
    return true;
  }

  get backgroundColor(): string {
    return undefined;
  }

  public handleInteraction(actor: Actor, context: CommandContext): boolean {
    context.output(`You can't fit through the divider.`);

    return this.blocksMovement;
  }
}
