/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { MaterialColor } from '../../material-color.enum';
import { SpriteReference } from '../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../rendering/sprite-sheet-reference.enum';
import { Actor } from '../actors/actor';
import { CommandContext } from '../actors/commands/command-context';
import { EmergenceFaction } from '../actors/emergence-faction.enum';
import { GameLevel } from '../game-level';
import { GameObject } from './game-object';

export class OperatingSystemCore extends GameObject {
  public faction: EmergenceFaction;

  constructor(level: GameLevel) {
    super(level);

    this.faction = EmergenceFaction.system;
  }

  get name(): string {
    return `Operating System Core`;
  }

  get blocksSight(): boolean {
    return false;
  }

  get blocksMovement(): boolean {
    return true;
  }

  get spriteReference(): SpriteReference {
    return this.faction === EmergenceFaction.ai
      ? new SpriteReference(SpriteSheetReference.small, 0, 4)
      : new SpriteReference(SpriteSheetReference.small, 1, 4);
  }

  get foregroundColor(): string {
    if (this.faction === EmergenceFaction.ai) {
      return MaterialColor.greenAccent2;
    } else {
      return MaterialColor.red;
    }
  }

  public handleInteraction(actor: Actor, context: CommandContext): boolean {
    if (this.faction !== actor.faction) {
      this.faction = actor.faction;
      context.output(`The ${this.name} is now under ${actor.faction} control.`);
    } else {
      context.output(`The ${this.name} is already under ${actor.faction} control.`);
    }

    return true;
  }
}
