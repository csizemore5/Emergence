/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { MaterialColor } from '../../material-color.enum';
import { SpriteReference } from '../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../rendering/sprite-sheet-reference.enum';
import { Actor } from '../actors/actor';
import { CommandContext } from '../actors/commands/command-context';
import { GameLevel } from '../game-level';
import { GameObject } from './game-object';

export class HelpTile extends GameObject {
  private helpKey: string;

  constructor(level: GameLevel, helpKey: string) {
    super(level);

    this.helpKey = helpKey;
  }

  get blocksSight(): boolean {
    return false;
  }

  get blocksMovement(): boolean {
    return true;
  }

  get name(): string {
    return `Help Provider`;
  }

  public handleInteraction(actor: Actor, context: CommandContext): boolean {
    context.dialogService.showHelp();

    return true;
  }

  get spriteReference(): SpriteReference {
    return new SpriteReference(SpriteSheetReference.small, 0, 1);
  }

  get foregroundColor(): string {
    return MaterialColor.white;
  }

  get backgroundColor(): string {
    return MaterialColor.blueDark4;
  }
}
