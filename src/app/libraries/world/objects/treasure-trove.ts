/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { MaterialColor } from '../../material-color.enum';
import { SpriteReference } from '../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../rendering/sprite-sheet-reference.enum';
import { Actor } from '../actors/actor';
import { CommandContext } from '../actors/commands/command-context';
import { GameLevel } from '../game-level';
import { DestructibleObject } from './destructible-object';
import { GameObject } from './game-object';

export class TreasureTrove extends DestructibleObject {
  private isSearched: boolean = false;

  constructor(level: GameLevel) {
    super(level, 3);
  }

  get name(): string {
    return 'Data Cache';
  }

  get foregroundColor(): string {
    return MaterialColor.tealAccent4;
  }

  get blocksSight(): boolean {
    return true;
  }

  get blocksMovement(): boolean {
    return !this.isSearched;
  }

  get backgroundColor(): string {
    return undefined;
  }

  public handleInteraction(actor: Actor, context: CommandContext): boolean {
    if (!this.isSearched) {
      const loot: GameObject = context.bestiaryService.generateSingleLoot(context.level);
      if (loot) {
        this.level.addObject(loot, this.pos);
      } else {
        context.output(`There is nothing of interest here.`);
      }
      this.isSearched = true;

      return true;
    } else {
      return false;
    }
  }

  get spriteReference(): SpriteReference {
    return this.isSearched ? new SpriteReference(SpriteSheetReference.small, 2, 2) : new SpriteReference(SpriteSheetReference.small, 0, 2);
  }
}
