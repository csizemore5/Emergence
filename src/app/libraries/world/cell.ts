/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { FloorType } from '../../services/generation-service/floor-type.enum';
import { SpriteReference } from '../rendering/sprite-reference';
import { SpriteSheetReference } from '../rendering/sprite-sheet-reference.enum';
import { Actor } from './actors/actor';
import { GameLevel } from './game-level';
import { GameObject } from './objects/game-object';
import { Point } from './point';

export class Cell {
  public level: GameLevel;
  public floorType: FloorType;
  public terrainName: string;
  public actor: Actor = null;
  public pos: Point = new Point(0, 0);

  constructor(cell: Cell = null) {
    if (cell) {
      this.level = cell.level;
      this.floorType = cell.floorType;
      this.terrainName = cell.terrainName;
      this.terrainName = cell.terrainName;
      this.pos = new Point(cell.pos.x, cell.pos.y);
      this.actor = cell.actor;
    }
  }

  get spriteReference(): SpriteReference {
    switch (this.floorType) {
      case FloorType.CautionMarker:
        return new SpriteReference(SpriteSheetReference.small, 12, 0);
      case FloorType.DecorativeTile:
        return new SpriteReference(SpriteSheetReference.small, 15, 0);
      case FloorType.Normal:
        return new SpriteReference(SpriteSheetReference.small, 13, 0);
      case FloorType.Walkway:
        return new SpriteReference(SpriteSheetReference.small, 10, 0);
      case FloorType.Void:
      default:
        return null;
    }
  }

  get objects(): GameObject[] {
    return this.level.getObjectsAtPos(this.pos);
  }

  get isImpassible(): boolean {
    return this.objects.filter((o: GameObject): boolean => o.blocksMovement).length > 0 || this.floorType === FloorType.Void;
  }

  get hasInteractiveObjects(): boolean {
    return this.objects.length > 0;
  }

  public allowAiMovement(actor: Actor): boolean {
    for (const obj of this.objects) {
      if (!obj.allowAiMovement(actor)) {
        return false;
      }
    }

    return true;
  }

  public get generatesSightBlocker(): boolean {
    return (this.actor && this.actor.blocksSight) || this.objects.filter((o: GameObject): boolean => o.blocksSight).length > 0;
  }
}
