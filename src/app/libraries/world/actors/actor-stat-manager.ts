/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {Actor} from './actor';
import {ActorStat} from './actor-stat';
import {IStatModifier} from './i-stat-modifier';

export class ActorStatManager {
  private stats: ActorStat[];
  private modifiers: Set<IStatModifier>;

  constructor(private actor: Actor) {
    this.stats = [];
    this.modifiers = new Set<IStatModifier>();
  }

  public incrementStat(id: string, amount: number = 1): number {
    let stat: ActorStat;
    if (this.stats[id]) {
      stat = this.stats[id];

      return stat.increment(amount);
    } else {
      stat = new ActorStat(id, amount);
      this.stats[id] = stat;

      return amount;
    }
  }

  public calculate(id: string): number {
    const stat: ActorStat = this.stats[id];

    let value: number = 0;
    if (stat) {
      value = stat.current;
    }

    this.modifiers.forEach((m: IStatModifier): number => (value += m.getModifierForStat(id, this.actor)));

    if (stat) {
      return Math.min(stat.max, Math.max(stat.min, value));
    } else {
      return value;
    }
  }

  public register(stat: ActorStat): void {
    if (stat && stat.id) {
      this.stats[stat.id] = stat;
    }
  }

  public registerModifier(command: IStatModifier): void {
    this.modifiers.add(command);
  }

  public unregisterModifier(command: IStatModifier): boolean {
    return this.modifiers.delete(command);
  }
}
