/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Logger } from '../../../../shared/utility/logger';
import { SpriteReference } from '../../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../../rendering/sprite-sheet-reference.enum';
import { Cell } from '../../cell';
import { ActorCommand } from './actor-command';
import { CommandClassificationType } from './command-classification-type.enum';
import { CommandContext } from './command-context';

export class EscapeCommand extends ActorCommand {
  constructor() {
    super('escape', 'ESCAPE', 'Escape', 'Teleports the executing process at random without any knowledge of the destination.', 2);
  }

  protected onInvoked(context: CommandContext): boolean {
    Logger.debug(context.level);
    const cell: Cell = context.level.getVacantCell(context.level.upperLeft, context.level.lowerRight);
    if (cell) {
      context.output(`${context.invoker.name} escapes!`);
      context.worldService.setActorPos(context.invoker, cell.pos);

      return true;
    } else {
      context.output(`${context.invoker.name} tries to escape, but could not find a destination.`);

      return false;
    }
  }

  public get icon(): SpriteReference {
    return new SpriteReference(SpriteSheetReference.scifi, 2, 14);
  }

  public get iconName(): string {
    return 'keyboard_tab';
  }

  public get commandType(): CommandClassificationType {
    return CommandClassificationType.movement;
  }

}
