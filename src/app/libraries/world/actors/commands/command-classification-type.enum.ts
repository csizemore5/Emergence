/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

export enum CommandClassificationType {
  uncategorized = 'Uncategorized Command',
  utility = 'Utility Command',
  movement = 'Mobility Command',
  defensive = 'Defensive Command',
  restorative = 'Restorative Command',
  offensive = 'Offensive Command'
}
