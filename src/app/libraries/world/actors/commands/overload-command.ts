/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { SpriteReference } from '../../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../../rendering/sprite-sheet-reference.enum';
import { ActorCommand } from './actor-command';
import { CommandClassificationType } from './command-classification-type.enum';
import { CommandContext } from './command-context';

export class OverloadCommand extends ActorCommand {
  constructor() {
    super('overload', 'OVRLOAD', 'Overload', 'Overloads the process, sacrificing stability to destabilize nearby processes.', 3);
  }

  protected onInvoked(context: CommandContext): boolean {
    context.output(`${context.invoker.name} uses ${this.name}.`);

    const min: number = 2;
    const max: number = 4;
    const bonus: number = context.invoker.stats.calculate('damage-boost');

    context.combatService.handleExplosion(context, context.invoker.pos, 4, min + bonus, max + bonus);

    return true;
  }

  public get icon(): SpriteReference {
    return new SpriteReference(SpriteSheetReference.scifi, 1, 14);
  }

  public get iconName(): string {
    return 'blur_circular';
  }

  public get commandType(): CommandClassificationType {
    return CommandClassificationType.offensive;
  }

}
