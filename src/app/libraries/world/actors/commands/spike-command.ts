/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { SpriteReference } from '../../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../../rendering/sprite-sheet-reference.enum';
import { Cell } from '../../cell';
import { CommandClassificationType } from './command-classification-type.enum';
import { CommandContext } from './command-context';
import { TargetedCommand } from './targeted-command';

export class SpikeCommand extends TargetedCommand {
  constructor() {
    super('spike', 'SPIKE', 'Spike', `Sends a spike to the target, causing damage to the process' stability.`, 2);

    this.maxRange = 4.5;
  }

  public checkValidTarget(cell: Cell): string {
    const superResult: string = super.checkValidTarget(cell);
    if (superResult) {
      return superResult;
    }

    if (!this.executionContext.invoker.canSee(cell.pos)) {
      return `You need a direct line of sight to use ${this.name}.`;
    }

    if (!cell.actor) {
      return `There's nothing there to attack.`;
    }

    if (cell.actor === this.executionContext.invoker) {
      return `You can't use ${this.name} on yourself!`;
    }

    return undefined;
  }

  protected onInvoked(context: CommandContext): boolean {
    context.output(`${context.invoker.name} uses ${this.name}.`);

    context.combatService.handleRangedAttack(context, this.target.actor);

    return true;
  }

  public get icon(): SpriteReference {
    return new SpriteReference(SpriteSheetReference.scifi, 6, 14);
  }

  public get iconName(): string {
    return 'call_missed_outgoing';
  }

  public get commandType(): CommandClassificationType {
    return CommandClassificationType.offensive;
  }
}
