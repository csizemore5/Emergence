/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { ActorCommand } from './actor-command';

/**
 * Represents an available slot that an actor can use to either store or host an active command.
 */
export class CommandSlot {
  public command: ActorCommand;
  public shortcutKey: string;
  public isInCommandBar: boolean;

  constructor(isInCommandBar: boolean, command: ActorCommand = null) {
    this.command = command;
  }
}
