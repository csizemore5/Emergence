/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { ActorCommand } from './actor-command';
import { CommandClassificationType } from './command-classification-type.enum';
import { CommandContext } from './command-context';

export class RecallCommand extends ActorCommand {
  constructor() {
    super('recall', 'RECALL', 'Recall', 'Teleports to the previously stored location (set by the MARK command)', 2);
  }

  protected onInvoked(context: CommandContext): boolean {
    if (context.invoker.markedPos) {
      context.output(`${context.invoker.name} recalls their last marked position.`);
      context.worldService.setActorPos(context.invoker, context.invoker.markedPos);

      return true;
    } else {
      context.output(`${context.invoker.name} cannot recall without having marked somewhere on this machine first.`);

      return false;
    }
  }

  public get iconName(): string {
    return 'keyboard_return';
  }

  public get commandType(): CommandClassificationType {
    return CommandClassificationType.movement;
  }
}
