/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { SpriteReference } from '../../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../../rendering/sprite-sheet-reference.enum';
import { Cell } from '../../cell';
import { CommandPickup } from '../../objects/command-pickup';
import { GameObject } from '../../objects/game-object';
import { NetworkPort } from '../../objects/network-port';
import { Point } from '../../point';
import { Actor } from '../actor';
import { CommandClassificationType } from './command-classification-type.enum';
import { CommandContext } from './command-context';
import { TargetedCommand } from './targeted-command';

export class SwapCommand extends TargetedCommand {
  constructor() {
    super('swap', 'SWAP', 'Swap', `Swaps your location with whatever is present at the target position.`, 1);
    this.maxRange = 5.5;
  }

  public checkValidTarget(cell: Cell): string {
    const superResult: string = super.checkValidTarget(cell);
    if (superResult) {
      return superResult;
    }

    if (!cell || cell.isImpassible) {
      return `You cannot swap places with impassible terrain`;
    }

    if (cell.objects.filter((o: GameObject): boolean => o.blocksMovement).length > 0) {
      return `Immovable terrain blocks your path.`;
    }

    if (cell.objects.filter((o: GameObject): boolean => o instanceof NetworkPort).length > 0) {
      return `Localized fields prevent you from swapping with this location.`;
    }

    if (!this.executionContext.invoker.canSee(cell.pos)) {
      return `You can't see that location.`;
    }

    return undefined;
  }

  protected onInvoked(context: CommandContext): boolean {
    if (!this.target) {
      throw new Error(`Invoked command without a target being set`);
    }

    context.output(`${context.invoker.name} runs ${this.name}.`);

    const oldPos: Point = new Point(context.invoker.pos.x, context.invoker.pos.y);
    const oldActor: Actor = this.target.actor;

    context.worldService.setActorPos(context.invoker, this.target.pos);

    if (oldActor) {
      context.worldService.setActorPos(oldActor, oldPos);

      // The actor at the destination gets hurt
      let message: string = context.combatService.hurtActor(context, oldActor, 1, context.invoker);
      if (message) {
        context.messageService.showMessage(message);
      }

      // The teleportation accident injures the command invoker as well
      message = context.combatService.hurtActor(context, context.invoker, 1, context.invoker);
      if (message) {
        context.messageService.showMessage(message);
      }
    }

    for (const object of this.target.objects) {
      if (object instanceof CommandPickup) {
        context.worldService.setObjectPos(object, oldPos);
      }
    }

    return true;
  }

  public get icon(): SpriteReference {
    return new SpriteReference(SpriteSheetReference.scifi, 12, 14);
  }

  public get iconName(): string {
    return 'swap_horiz';
  }

  public get commandType(): CommandClassificationType {
    return CommandClassificationType.movement;
  }
}
