/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { ActorCommand } from './actor-command';
import { CommandClassificationType } from './command-classification-type.enum';
import { CommandContext } from './command-context';

export class MarkCommand extends ActorCommand {
  constructor() {
    super(
      'mark',
      'MARK',
      'Mark',
      `Marks the process's current location. This position can be teleported to from anywhere on this machine via the RECALL command. ` +
        `Only one location per machine can be Marked at one time.`,
      2
    );
  }

  protected onInvoked(context: CommandContext): boolean {
    context.output(`${context.invoker.name} marks their current location for future recall.`);
    context.invoker.markedPos = context.invoker.pos;

    return true;
  }

  public get iconName(): string {
    return 'label_outline';
  }

  public get commandType(): CommandClassificationType {
    return CommandClassificationType.utility;
  }
}
