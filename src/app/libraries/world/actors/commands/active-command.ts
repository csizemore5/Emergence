/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { GoogleAnalyticsHelper } from '../../../../shared/utility/google-analytics-helper';
import { Actor } from '../actor';
import { IStatModifier } from '../i-stat-modifier';
import { ActorCommand } from './actor-command';
import { CommandContext } from './command-context';

export abstract class ActiveCommand extends ActorCommand implements IStatModifier {
  public isActive: boolean = false;
  public turnsActive: number = 0;

  private readonly statModifiers: number[];

  protected constructor(id: string, label: string, name: string, description: string, activationCost: number) {
    super(id, label, name, description, activationCost);

    this.statModifiers = [];
  }

  public get activationCostText(): string {
    return this.activationCost === 1 ? `1 Op / Turn` : `${this.activationCost} Ops / Turn`;
  }

  public update(context: CommandContext): void {
    this.turnsActive++;
  }

  public getModifierForStat(id: string, actor: Actor): number {
    if (this.isActive && this.statModifiers[id]) {
      return this.statModifiers[id];
    }

    return 0;
  }

  public invoke(context: CommandContext): boolean {
    if (this.isActive) {
      context.output(`${context.invoker.name} deactivates ${this.name}.`);
      if (context.isPlayerAction) {
        GoogleAnalyticsHelper.emitEvent('Command', 'Deactivate', this.name, this.turnsActive);
      }
    } else {
      context.output(`${context.invoker.name} activates ${this.name}.`);
      if (context.isPlayerAction) {
        GoogleAnalyticsHelper.emitEvent('Command', 'Activate', this.name);
      }
      this.turnsActive = 0;
    }
    this.isActive = !this.isActive;

    return this.isActive;
  }

  protected modifiesStat(statId: string, amount: number): void {
    this.statModifiers[statId] = amount;
  }
}
