/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { IHelpBody } from '../../../help/i-help-body';
import { SpriteSheetReference } from '../../rendering/sprite-sheet-reference.enum';

export interface IActorDefinition {
  template: string;
  spriteX: number;
  spriteY: number;
  spriteSheet: SpriteSheetReference;
  spriteTint: string;
  captureOnKill: boolean;
  id: string;
  name: string;
  character: string;
  hp: number;
  mp: number;
  commands: string[];
  visionRadius: number;
  opsRegen: number;
  damageResistance: number;
  blocksSight: boolean;
  maxAttack: number;
  minAttack: number;
  side: string;
  accuracy: number;
  evade: number;
  immobile: boolean;
  killOnAttack: boolean;
  explosionRadius: number;
  explosionMinDamage: number;
  explosionMaxDamage: number;
  help: IHelpBody[];
}
