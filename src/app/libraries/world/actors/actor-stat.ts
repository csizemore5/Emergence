/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

export class ActorStat {
  public id: string;
  public min: number = 0;
  public max: number = 100;
  public current: number = 100;

  constructor(id: string, value: number, maxValue: number = 100, minValue: number = 0) {
    this.id = id;
    this.max = maxValue;
    this.current = value;
    this.min = minValue;
  }

  public increment(amount: number = 1): number {

    this.current = Math.min(this.current + amount, this.max);

    return this.current;
  }

}
