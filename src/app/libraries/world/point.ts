/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { isNullOrUndefined } from 'util';

export class Point {
  public x: number;
  public y: number;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }

  public equals(otherPoint: Point): boolean {
    return otherPoint && this.x === otherPoint.x && this.y === otherPoint.y;
  }

  public isAdjacentTo(otherPoint: Point, allowDiagonal: boolean): boolean {
    if (isNullOrUndefined(otherPoint)) {
      return false;
    }

    const xDiff: number = Math.abs(otherPoint.x - this.x);
    const yDiff: number = Math.abs(otherPoint.y - this.y);

    if (allowDiagonal) {
      return xDiff <= 1 && yDiff <= 1;
    } else {
      return (xDiff === 1 && yDiff === 0) || (xDiff === 0 && yDiff === 1);
    }
  }

  public add(amount: Point): Point {
    return new Point(this.x + amount.x, this.y + amount.y);
  }

  public subtract(amount: Point): Point {
    return new Point(this.x - amount.x, this.y - amount.y);
  }

  public offset(x: number, y: number): Point {
    return this.add(new Point(x, y));
  }

  public toString(): string {
    return `(${this.x}, ${this.y})`;
  }

  public calculateDistanceFrom(pos: Point): number {
    const a: number = pos.x - this.x;
    const b: number = pos.y - this.y;

    // c^2 = a^2 + b^2
    return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
  }

  public static fromPos2D(pos: string): Point {
    if (!pos) {
      throw new Error('Cannot create a Point from a null/undefined IPos2D instance');
    }

    const strings: string[] = pos.split(',');

    return new Point(Number(strings[0]), Number(strings[1]));
  }
}
