/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { isNullOrUndefined, isUndefined } from 'util';
import { environment } from '../../../environments/environment';
import { FloorType } from '../../services/generation-service/floor-type.enum';
import { GameLevelType } from '../../services/generation-service/game-level-type.enum';
import { ICellData } from '../../services/generation-service/i-cell-data';
import { ILevel } from '../../services/generation-service/i-level';
import { Logger } from '../../shared/utility/logger';
import { Randomizer } from '../randomizer';
import { Actor } from './actors/actor';
import { PlayerActor } from './actors/player-actor';
import { Cell } from './cell';
import { CellRow } from './cell-row';
import { GameObject } from './objects/game-object';
import { Point } from './point';

export class GameLevel {
  public name: string = 'Untitled level';
  public type: GameLevelType;

  public actors: Actor[] = [];
  public readonly rows: CellRow[] = [];
  public readonly objects: GameObject[] = [];

  public isCompleted: boolean = false;

  public minX: number;
  public maxX: number;
  public minY: number;
  public maxY: number;

  private _width: number;
  private _height: number;
  private readonly objectsByPos: GameObject[][];
  public playerStartPos: Point;

  constructor(data: ILevel, levelType: GameLevelType) {
    this.name = data.name;
    this.playerStartPos = Point.fromPos2D(data.playerStart);
    this.type = levelType;

    this.objects = [];
    this.objectsByPos = [];
  }

  public get hasAdminAccess(): boolean {
    const player: PlayerActor = <PlayerActor>this.actors.find((a: Actor): boolean => a.isPlayer);

    if (!player) {
      return false;
    }

    return player.hasAdminAccess;
  }

  public buildEmptyCell(cellPos: Point): Cell {
    const emptyCell: Cell = new Cell();
    emptyCell.pos = new Point(cellPos.x, cellPos.y);
    emptyCell.level = this;
    emptyCell.terrainName = '';
    emptyCell.floorType = FloorType.Void;
    emptyCell.actor = this.getActorAtCell(cellPos);

    return emptyCell;
  }

  get width(): number {
    return this._width;
  }

  get height(): number {
    return this._height;
  }

  public clearDeadActors(): Actor[] {
    this.actors = this.actors.filter((a: Actor): boolean => !a.isDead());

    return this.actors;
  }

  public getActorAtCell(cellPos: Point): Actor {
    for (const actor of this.actors) {
      if (actor.pos.equals(cellPos)) {
        return actor;
      }
    }

    return null;
  }

  public getCell(cellPos: Point): Cell {
    if (!this.rows[cellPos.y]) {
      return this.buildEmptyCell(cellPos);
    }

    const actualCell: Cell = this.rows[cellPos.y].cells[cellPos.x];
    if (actualCell) {
      actualCell.actor = this.getActorAtCell(cellPos);

      return actualCell;
    } else {
      return this.buildEmptyCell(cellPos);
    }
  }

  /**
   * Gets a 3x3 grid of cells with the requested position in the center of the grid
   * @param {Point} center
   * @param {boolean} includeCenter whether or not the center of the grid is included in the results
   * @param {boolean} includeDiagonal whether or not diagonal spots from the center of the grid will be included in the results
   * @param radius {number} The range + or - that will be drawn into the cell tile grid. By default this is 1, but this can be widened.
   * @returns {Cell[]}
   */
  public getCellGrid(center: Point, includeCenter: boolean = true, radius: number = 1, includeDiagonal: boolean = true): Cell[] {
    const cells: Cell[] = [];

    // Loop through a 3x3 grid around the requested point
    for (let y: number = center.y - radius; y <= center.y + radius; y++) {
      for (let x: number = center.x - radius; x <= center.x + radius; x++) {
        const point: Point = new Point(x, y);

        // Allow filtering out center tiles
        if (!includeCenter && point.equals(center)) {
          continue;
        }

        // Allow filtering out diagonal moves
        if (!includeDiagonal && x !== center.x && y !== center.y) {
          continue;
        }

        cells.push(this.getCell(point));
      }
    }

    return cells;
  }

  public finalize(): void {
    this.updateDimensions();

    // Tell the smart objects about each other
    for (const obj of this.objects.filter((o: GameObject): boolean => !isUndefined((<any>o).calculateSprite))) {
      (<any>obj).calculateSprite();
    }

    if (!environment.production) {
      let failed: boolean = false;
      const actorPos: number[] = [];
      for (const actor of this.actors) {
        const key: string = actor.pos.toString();
        if (actorPos[key]) {
          actorPos[key] += 1;
          failed = true;
        } else {
          actorPos[key] = 1;
        }
      }

      if (failed) {
        Logger.error(
          `Multiple actors detected occupying the same space!`,
          actorPos,
          this.actors.map((a: Actor): string => a.pos.toString())
        );
      } else {
        Logger.debug(`Actor Check Passed`, this.actors.map((a: Actor): string => a.pos.toString()));
      }
    }
  }

  get upperLeft(): Point {
    if (isNullOrUndefined(this.minX) || isNullOrUndefined(this.minY)) {
      this.updateDimensions();
    }

    return new Point(this.minX, this.minY);
  }

  get lowerRight(): Point {
    if (isNullOrUndefined(this.maxX) || isNullOrUndefined(this.maxY)) {
      this.updateDimensions();
    }

    return new Point(this.maxX, this.maxY);
  }

  public getVacantCell(startOffset: Point, endOffset: Point, numTries: number = 25): Cell | null {
    let triesRemaining: number = numTries;

    // If we're using a negative number, allow it indefinitely
    while (triesRemaining-- !== 0) {
      // Get a random point within the room
      const x: number = Randomizer.getRandomNumber(startOffset.x, endOffset.x);
      const y: number = Randomizer.getRandomNumber(startOffset.y, endOffset.y);
      const pos: Point = new Point(x, y);

      const cell: Cell = this.getCell(pos);

      // If this is vacant, report that we can use it
      if (!cell.isImpassible && isNullOrUndefined(cell.actor) && cell.objects.length <= 0) {
        return cell;
      }
    }

    return null;
  }

  public addObject(obj: GameObject, pos: Point): void {
    this.objects.push(obj);
    obj.pos = pos;

    // Invalidate the cache
    if (this.objectsByPos[pos.toString()]) {
      this.objectsByPos[pos.toString()] = undefined;
    }
  }

  public removeObject(obj: GameObject): void {
    const index: number = this.objects.indexOf(obj);
    if (index >= 0) {
      // Invalidate the cache for next time around
      if (this.objectsByPos[obj.pos.toString()]) {
        this.objectsByPos[obj.pos.toString()] = undefined;
      }

      // Remove it from the master list
      this.objects.splice(index, 1);
    } else {
      Logger.error(`Could not remove object from cell`, this, obj);
    }
  }

  /**
   * Gets all GameObjects at the given position.
   * NOTE: This method is performance critical.
   * @param {Point} pos the position
   * @returns {GameObject[]} the game objects
   */
  public getObjectsAtPos(pos: Point): GameObject[] {
    // If we've already got it cached, then just grab it from there
    let cachedObjects: GameObject[] = this.objectsByPos[pos.toString()];
    if (cachedObjects) {
      return cachedObjects;
    }

    // Okay, we don't have it, so we'll need to load it, while storing this array in the cache for next time
    cachedObjects = [];
    this.objectsByPos[pos.toString()] = cachedObjects;

    for (const obj of this.objects.filter((o: GameObject): any => o.pos.equals(pos))) {
      cachedObjects.push(obj);
    }

    return cachedObjects;
  }

  private updateDimensions(): void {
    Logger.debug(`Updating level dimensions`);

    this.minX = undefined;
    this.maxX = undefined;
    this.minY = undefined;
    this.maxY = undefined;

    for (const rowId of Object.keys(this.rows)) {
      const rowIdInt: number = +rowId;
      if (isUndefined(this.minY) || rowIdInt < this.minY) {
        this.minY = rowIdInt;
      }
      if (isUndefined(this.maxY) || rowIdInt > this.maxY) {
        this.maxY = rowIdInt;
      }
    }

    for (const row of this.rows) {
      // Ensure we actually have cells in this row
      if (isNullOrUndefined(row) || isNullOrUndefined(row.cells)) {
        continue;
      }

      for (const cellId of Object.keys(row.cells)) {
        const cellIdInt: number = +cellId;
        if (isUndefined(this.minX) || cellIdInt < this.minX) {
          this.minX = cellIdInt;
        }
        if (isUndefined(this.maxX) || cellIdInt > this.maxX) {
          this.maxX = cellIdInt;
        }
      }
    }

    this._width = this.maxX - this.minX;
    this._height = this.maxY - this.minY;
  }

  public addCell(cellData: ICellData): void {
    const pos: Point = Point.fromPos2D(cellData.pos);

    // Ensure the row exists
    if (!this.rows[pos.y]) {
      this.rows[pos.y] = new CellRow();
    }

    // Build a concrete object from the DTO
    const cellObj: Cell = new Cell();
    cellObj.pos = Point.fromPos2D(cellData.pos);
    cellObj.level = this;
    cellObj.floorType = cellData.floorType;

    // Add the new object to the data
    this.rows[pos.y].cells[pos.x] = cellObj;
  }
}
