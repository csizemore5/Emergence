/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import * as Chance from 'chance';
import {isNullOrUndefined} from 'util';

export class Randomizer {
  private static _chance: Chance = null;

  private static get rng(): Chance {
    // Lazy instantiate the Chance library
    if (isNullOrUndefined(Randomizer._chance)) {
      Randomizer._chance = new Chance();
    }

    return Randomizer._chance;
  }

  public static getWeightedEntry(entries: any[], weights: number[]): any {
    return Randomizer.rng.weighted(entries, weights);
  }

  public static getRandomNumber(min: number, max: number): number {
    // Don't do anything expensive if this isn't really a choice
    if (max <= min) {
      return min;
    }

    return Randomizer.rng.integer({ min, max });
  }

  public static getRandomEntry(entries: any[], removeFromList: boolean = false): any {
    if (!entries) {
      return null;
    }

    const index: number = Randomizer.getRandomNumber(0, entries.length - 1);
    const entity: any = entries[index];

    if (removeFromList) {
      entries.splice(index, 1);
    }

    return entity;
  }

  /**
   * Gets a number between 0 and 100, for use in weight calculations
   * @returns {number}
   */
  public static getWholePercent(): number {
    return this.getRandomNumber(0, 100);
  }

  public static getRandomBoolean(): boolean {
    return Randomizer.rng.bool();
  }
}
