/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {isNullOrUndefined} from 'util';

export class ArrayHelper {
  public static removeIfPresent(items: any[], item: any): boolean {
    if (isNullOrUndefined(items)) {
      return false;
    }

    const index: number = items.indexOf(item);
    if (index < 0) {
      return false;
    }

    items.splice(index, 1);

    return true;
  }

  public static replaceElement(items: any[], search: any, replacement: any): any[] {
    // Special case early exit if we're not actually changing things
    if (search === replacement) {
      return items;
    }

    const index: number = items.indexOf(search);
    if (index < 0) {
      return items;
    }

    // Swap out elements entirely
    items[index] = replacement;

    // Because we could have multiple instances, we'll go recursive
    return this.replaceElement(items, search, replacement);
  }

  public static clone(arr: any[]): any[] {
    return arr.slice();
  }

  public static contains(contents: any[], entity: any): boolean {
    return !isNullOrUndefined(contents) && contents.indexOf(entity) >= 0;
  }
}
