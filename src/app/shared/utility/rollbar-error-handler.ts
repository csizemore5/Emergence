/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { ErrorHandler, Injectable, InjectionToken, Injector } from '@angular/core';
import * as Rollbar from 'rollbar';
import { environment } from '../../../environments/environment';
import { GoogleAnalyticsHelper } from './google-analytics-helper';
import { Logger } from './logger';

export let rollbarConfig: any = {
  accessToken: 'acc4e0491ac84fe08d8f51c243189178',
  captureUncaught: environment.production,
  captureUnhandledRejections: environment.production,
  enabled: environment.production,
  environment: environment.environmentName
};

export const RollbarService: InjectionToken<Rollbar> = new InjectionToken<Rollbar>('rollbar');

@Injectable()
export class RollbarErrorHandler implements ErrorHandler {
  public static instance: RollbarErrorHandler;

  constructor(private injector: Injector) {
    RollbarErrorHandler.instance = this;

    // Kinda wonky, but there's not another great place to hook these two up
    Logger.errorLogged.subscribe((err: string): void => GoogleAnalyticsHelper.emitException(err));
  }

  public handleError(err: any): void {
    Logger.error(err);

    const rollbar: Rollbar = this.injector.get(RollbarService);
    rollbar.error(err.originalError || err);
  }
}

export function rollbarFactory(): Rollbar {
  return new Rollbar(rollbarConfig);
}
