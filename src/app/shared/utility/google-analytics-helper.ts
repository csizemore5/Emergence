/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { isNullOrUndefined } from 'util';
import { environment } from '../../../environments/environment';
import { Logger } from './logger';

declare let ga: any;

/**
 * Represents parameters expected by the Google Analytics events API
 */
interface IGoogleAnalyticsEventParameters {
  eventCategory: string;
  eventLabel: string | undefined;
  eventAction: string;
  eventValue: number | undefined;
  appName: string;
  appVersion: string;
}

/**
 * Represents parameters expected by the Google Analytics screen API
 */
interface IGoogleAnalyticsScreenViewParameters {
  screenName: string;
  appName: string;
  appVersion: string;
}

/**
 * Helper class for interacting with Google Analytics
 */
export class GoogleAnalyticsHelper {
  /**
   * Logs an event to Google Analytics
   * @param {string} eventCategory the category of the event
   * @param {string} eventAction the event action
   * @param {string} eventLabel the event label
   * @param {number} eventValue a numeric representation for the event, if one is appropriate
   */
  public static emitEvent(eventCategory: string, eventAction: string, eventLabel: string = null, eventValue: number = null): void {
    // If we're non-analytics, just get out of here
    if (!environment.enableAnalytics) {
      return;
    }

    // Make sure that GA was successfully registered via the Google Analytics JS snippet.
    if (!ga) {
      Logger.warn('Cannot send analytics event without ga defined.', arguments);

      return;
    }

    // Build out the parameters to send over
    const gaParams: IGoogleAnalyticsEventParameters = {
      eventCategory,
      eventLabel,
      eventAction,
      eventValue,
      appName: 'Emergence',
      appVersion: environment.version
    };

    Logger.debug(`Logging event`, gaParams);

    // Actually send the event. This relies on GA being defined at the main containing page from the JS snippet from Google Analytics
    ga('send', 'event', gaParams);
  }

  /**
   * Logs a screen view to Google Analytics
   * @param {string} screenName the name of the screen viewed.
   */
  public static emitScreenView(screenName: string): void {
    // If we're non-analytics, just get out of here
    if (!environment.enableAnalytics) {
      return;
    }

    // Make sure that GA was successfully registered via the Google Analytics JS snippet.
    if (!ga) {
      Logger.warn('Cannot send analytics screen view without ga defined.', arguments);

      return;
    }

    // Build out the parameters to send over
    const gaParams: IGoogleAnalyticsScreenViewParameters = {
      screenName,
      appName: 'Emergence',
      appVersion: environment.version
    };

    Logger.debug(`Logging screen view`, gaParams);

    // Actually send the screen view. This relies on GA being defined at the main containing page from the JS snippet from Google Analytics
    ga('send', 'screenview', gaParams);

    // Events are nice for GA diagnostics
    this.emitEvent('Navigation', 'Page', screenName);
  }

  /**
   * Sends an exception to Google Analytics for change tracking
   * @param {string} description the description of the exception
   * @param {boolean} isFatal whether or not the exception was fatal
   */
  public static emitException(description: string, isFatal: boolean = false): void {
    // If we're non-analytics, just get out of here
    if (!environment.enableAnalytics || isNullOrUndefined(description)) {
      return;
    }

    // Make sure that GA was successfully registered via the Google Analytics JS snippet.
    if (!ga) {
      Logger.warn('Cannot send analytics exception without ga defined.', arguments);

      return;
    }

    const exceptionDetails: Object = {
      exDescription: description,
      exFatal: isFatal
    };

    Logger.debug(`Sending exception to Google Analytics`, exceptionDetails);

    ga('send', 'exception', exceptionDetails);
  }
}
