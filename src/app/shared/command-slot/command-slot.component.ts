/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { CommandButtonInstruction } from '../../libraries/rendering/command-button-instruction';
import { CommandSlot } from '../../libraries/world/actors/commands/command-slot';
import { TooltipService } from '../../services/ui-service/tooltip.service';
import { Logger } from '../utility/logger';

@Component({
  selector: 'em-command-slot',
  templateUrl: './command-slot.component.html',
  styleUrls: ['./command-slot.component.scss']
})
export class CommandSlotComponent implements OnInit, OnDestroy {
  @Input() public isSmall: boolean = false;
  @Input() public slot: CommandSlot;
  @Input() public isMainUI: boolean = false;
  @Input() public isSortable: boolean = false;
  @Input() public selected: boolean = false;
  @Input() public hotkey: any;
  @Input() public enabled: boolean = true;
  @Input() public callback: () => void = null;
  @Input() public instruction: CommandButtonInstruction = null;
  @Input() public isSelectable: boolean = true;

  constructor(private tooltipService: TooltipService) {}

  public ngOnInit(): void {}

  public onClicked(event: MouseEvent): void {
    if (this.isMainUI && this.slot && this.callback && this.enabled) {
      Logger.debug(`Mouse click event`, event);
      this.callback();
    }
  }

  public get canShowTooltip(): boolean {
    return this.isMainUI && this.enabled && !isNullOrUndefined(this.instruction);
  }

  public onMouseLeave(event: MouseEvent): void {
    if (this.canShowTooltip) {
      Logger.debug(`Mouse leave event`, event);
      this.tooltipService.cancelTooltip(this.instruction);
    }
  }

  public ngOnDestroy(): void {
    if (this.canShowTooltip) {
      this.tooltipService.cancelTooltip(this.instruction);
    }
  }

  public onMouseMove(event: MouseEvent): void {
    // Mouse move is fine here since we do a downstream isToolTip check
    if (this.canShowTooltip) {
      Logger.debug(`Mouse move event`, event);
      this.tooltipService.requestTooltip(this.instruction);
    }
  }
}
