/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActorDefinitionStatsHelpComponent } from './actor-definition-stats-help.component';

describe('ActorDefinitionStatsHelpComponent', () => {
  let component: ActorDefinitionStatsHelpComponent;
  let fixture: ComponentFixture<ActorDefinitionStatsHelpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActorDefinitionStatsHelpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActorDefinitionStatsHelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
