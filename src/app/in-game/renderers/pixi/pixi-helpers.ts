/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import * as PIXI from 'pixi.js';
import { ColorHelper } from '../../../libraries/color-helper';
import { MaterialColor } from '../../../libraries/material-color.enum';
import { ITooltipProvider } from '../../../libraries/rendering/i-tooltip-provider';
import { RenderInstruction } from '../../../libraries/rendering/render-instruction';
import { SpriteReference } from '../../../libraries/rendering/sprite-reference';
import { Point } from '../../../libraries/world/point';
import { Logger } from '../../../shared/utility/logger';
import { PixiRenderingDirector } from './pixi-rendering-director';
import TextStyle = PIXI.TextStyle;
import TextStyleOptions = PIXI.TextStyleOptions;

export class PixiHelpers {
  public static buildPixiRectangle(pos: Point, size: Point, isFilled: boolean, color: string): PIXI.Graphics {
    const pixiColor: number = ColorHelper.getHexColorFromString(color);
    const effect: PIXI.Graphics = new PIXI.Graphics();
    if (isFilled) {
      effect.beginFill(pixiColor);
    } else {
      effect.lineStyle(2, pixiColor);
      effect.fillAlpha = 0;
    }

    effect.drawRect(pos.x, pos.y, size.x, size.y);
    effect.endFill();

    return effect;
  }

  public static getTextOptions(color: string, size: number, wrapLength: number = 0): TextStyleOptions {
    return {
      fill: color,
      fontSize: size,
      fontFamily: 'Share Tech Mono',
      wordWrap: wrapLength && wrapLength > 0,
      wordWrapWidth: wrapLength,
      dropShadow: true,
      dropShadowDistance: 2,
      dropShadowColor: ColorHelper.getFadedColor(color)
    };
  }

  public static getTextStyle(color: string, size: number): TextStyle {
    return new TextStyle(this.getTextOptions(color, size));
  }

  public static addTooltipHandlers(container: PIXI.Container, instr: RenderInstruction, tooltipProvider: ITooltipProvider): void {
    // Tell Pixi this cares about mouse events
    container.interactive = true;

    // Listen to button events
    container.on('pointerenter', () => tooltipProvider.requestTooltip(instr));
    container.on('pointerout', () => tooltipProvider.cancelTooltip(instr));
  }

  public static buildSprite(
    director: PixiRenderingDirector,
    spriteReference: SpriteReference,
    size: Point,
    tint: string = ''
  ): PIXI.Sprite {
    const sprite: PIXI.Sprite = director.getSprite(spriteReference);

    // Apply coloration as needed
    if (tint && tint.toUpperCase() !== MaterialColor.white) {
      sprite.tint = ColorHelper.getHexColorFromString(tint);
    }

    const tileSize: number = sprite.width;

    // Scale the thing based on our instructed size
    if (size.x !== tileSize) {
      sprite.scale.x = size.x / tileSize;
    }
    if (size.y !== tileSize) {
      sprite.scale.y = size.y / tileSize;
    }
    if (spriteReference.opacity < 1) {
      sprite.alpha = spriteReference.opacity;
    }

    return sprite;
  }

  public static centerText(text: PIXI.Text, startPixel: Point, size: Point): void {
    text.position.set(startPixel.x + size.x / 2 - text.width / 2, startPixel.y);
  }

  public static rightAlignText(text: PIXI.Text, startPixel: Point, size: Point): void {
    text.position.set(startPixel.x + size.x - text.width, startPixel.y);
  }

  public static handleClickEvent(instr: RenderInstruction): void {
    if (instr.clickFunction) {
      Logger.debug(`Invoking click function`, instr);
      instr.clickFunction();
    } else {
      Logger.warn(`Clicked but no click function is specified.`, instr);
    }
  }
}
