/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import * as PIXI from 'pixi.js';
import {isNullOrUndefined} from 'util';
import {RectangleInstruction} from '../../../libraries/rendering/rectangle-instruction';
import {PixiHelpers} from './pixi-helpers';
import {PixiInstructionRenderer} from './pixi-instruction-renderer';
import {PixiRenderingDirector} from './pixi-rendering-director';

export class PixiRectangleRenderer extends PixiInstructionRenderer {
  public getContainer(director: PixiRenderingDirector, instruction: RectangleInstruction): PIXI.Container {
    const container: PIXI.Container = new PIXI.Container();
    container.name = `Rect`;
    director.addContainer(container, instruction.renderingLayer);

    return container;
  }

  public render(director: PixiRenderingDirector, instr: RectangleInstruction, container: PIXI.Container): void {
    const color: string = instr.bgColor;
    if (!isNullOrUndefined(color) && color !== '#000000') {
      const effect: PIXI.Graphics = PixiHelpers.buildPixiRectangle(instr.startPixel, instr.size, instr.isFilled, color);
      effect.alpha = instr.alpha;

      if (instr.fadeRate > 0 || instr.pulseDuration > 0) {
        director.addAnimations(instr, effect, color);
      }

      container.addChild(effect);
    }
  }
}
