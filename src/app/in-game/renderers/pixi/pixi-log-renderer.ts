/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import * as PIXI from 'pixi.js';
import { MaterialColor } from '../../../libraries/material-color.enum';
import { LogRenderInstruction } from '../../../libraries/rendering/log-render-instruction';
import { RenderInstruction } from '../../../libraries/rendering/render-instruction';
import { Point } from '../../../libraries/world/point';
import { PixiHelpers } from './pixi-helpers';
import { PixiInstructionRenderer } from './pixi-instruction-renderer';
import { PixiRenderingDirector } from './pixi-rendering-director';
import Container = PIXI.Container;

export class PixiLogRenderer extends PixiInstructionRenderer {
  private logContainer: PIXI.Container;

  public getContainer(director: PixiRenderingDirector, instruction: RenderInstruction): PIXI.Container {

    if (!this.logContainer) {
      this.logContainer = director.logContainer;
    }

    if (!this.logContainer) {
      this.logContainer = new Container();
      director.addContainer(this.logContainer, instruction.renderingLayer);
      director.logContainer = this.logContainer;
    } else {
      this.logContainer.removeChildren();
    }

    return this.logContainer;
  }

  public render(director: PixiRenderingDirector, instr: LogRenderInstruction, container: PIXI.Container): void {
    const bgSize: Point = new Point(0, 0);

    const fontSize: number = 18;
    let y: number = instr.startPixel.y;
    for (const message of instr.messages) {
      const text: PIXI.Text = new PIXI.Text(message, PixiHelpers.getTextStyle(instr.bgColor, fontSize));
      if (text.width > bgSize.x) {
        bgSize.x = text.width;
      }
      bgSize.y = y + text.height;
      text.position.set(instr.startPixel.x, y);
      container.addChild(text);

      y += +fontSize + 4;
    }

    const bg: PIXI.Graphics = PixiHelpers.buildPixiRectangle(instr.startPixel, bgSize, true, MaterialColor.black);
    bg.name = 'LogBackground';
    bg.alpha = 0.25;
    container.addChildAt(bg, 0);
  }
}
