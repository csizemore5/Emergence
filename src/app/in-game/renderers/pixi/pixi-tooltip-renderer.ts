/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import * as PIXI from 'pixi.js';
import { isNullOrUndefined } from 'util';
import { RenderInstruction } from '../../../libraries/rendering/render-instruction';
import { TooltipInfo } from '../../../libraries/rendering/tooltip-info';
import { TooltipRenderInstruction } from '../../../libraries/rendering/tooltip-render-instruction';
import { Point } from '../../../libraries/world/point';
import { FadeInAnimation } from '../animation/fade-in-animation';
import { PixiHelpers } from './pixi-helpers';
import { PixiInstructionRenderer } from './pixi-instruction-renderer';
import { PixiRenderingDirector } from './pixi-rendering-director';

export class PixiTooltipRenderer extends PixiInstructionRenderer {
  private container: PIXI.Container;

  private fadeAnimation: FadeInAnimation;

  public getContainer(director: PixiRenderingDirector, instruction: RenderInstruction): PIXI.Container {
    if (!this.container) {
      this.container = director.tooltipContainer;
    }

    if (!this.container) {
      this.container = new PIXI.Container();
      this.container.name = 'Tooltip';
      this.container.alpha = 0.95;
      director.addContainer(this.container, instruction.renderingLayer);
      director.tooltipContainer = this.container;
    } else {
      this.container.removeChildren();
    }

    this.fadeAnimation = new FadeInAnimation(this.container, 0, 0.95);

    return this.container;
  }

  public render(director: PixiRenderingDirector, instr: TooltipRenderInstruction, container: PIXI.Container): void {
    const info: TooltipInfo = instr.info;

    container.visible = !isNullOrUndefined(info);
    if (!info) {
      return;
    }

    const padding: number = 4;
    let leftOffset: number = 0;
    let minHeight: number = 0;

    let leftRenderer: PixiInstructionRenderer = null;

    if (info.renderThumbnail && instr.parent) {
      leftOffset = padding * 2 + instr.parent.size.x;
      minHeight = instr.parent.size.y + padding + padding;
      leftRenderer = director.rendererProvider.getRendererForInstruction(instr.parent);
    }

    const title: PIXI.Text = new PIXI.Text(info.title, PixiHelpers.getTextOptions(instr.foreColor, 16));
    const body: PIXI.Text = new PIXI.Text(info.body, PixiHelpers.getTextOptions(instr.foreColor, 14, 350));

    let rightHeader: PIXI.Text = null;
    if (info.rightHeaderText) {
      rightHeader = new PIXI.Text(info.rightHeaderText, PixiHelpers.getTextOptions(instr.foreColor, 12));
    }
    let footer: PIXI.Text = null;
    if (info.footer) {
      footer = new PIXI.Text(info.footer, PixiHelpers.getTextOptions(instr.foreColor, 12));
    }

    let footerHeight: number = 0;
    if (footer) {
      footerHeight = footer.height + padding * 2;
    }
    const size: Point = new Point(
      leftOffset + Math.max(title.width, body.width) + padding * 2,
      Math.max(title.height + body.height + padding * 3 + footerHeight, minHeight)
    );
    const pos: Point = PixiTooltipRenderer.calculateTooltipPosition(instr, padding, size, director);

    const bg: PIXI.Graphics = PixiHelpers.buildPixiRectangle(pos, size, true, instr.bgColor);
    bg.name = 'Tooltip Background';

    const border: PIXI.Graphics = PixiHelpers.buildPixiRectangle(pos, size, false, instr.foreColor);
    border.name = 'Tooltip Border';

    // Arrange the elements
    title.position.set(leftOffset + pos.x + padding, pos.y + padding);
    body.position.set(leftOffset + pos.x + padding, pos.y + padding * 2 + title.height);
    if (footer) {
      footer.position.set(leftOffset + pos.x + size.x - padding - footer.width, pos.y + size.y - padding - footer.height);
    }
    if (rightHeader) {
      rightHeader.position.set(leftOffset + pos.x + size.x - padding - rightHeader.width, pos.y + padding);
    }

    // Add everything to a central container
    container.addChild(bg);
    container.addChild(border);
    container.addChild(title);
    container.addChild(body);
    if (footer) {
      container.addChild(footer);
    }
    if (rightHeader) {
      container.addChild(rightHeader);
    }

    this.fadeAnimation.reset();
    director.animations.push(this.fadeAnimation);

    if (leftRenderer) {
      const clonedInstruction: RenderInstruction = { ...instr.parent };

      clonedInstruction.startPixel = new Point(pos.x + padding, pos.y + padding);
      clonedInstruction.isInToolTip = true;
      clonedInstruction.tooltip = null;

      const embeddedContainer: PIXI.Container = new PIXI.Container();
      embeddedContainer.name = 'TooltipRenderer';
      embeddedContainer.position.set(clonedInstruction.startPixel.x, clonedInstruction.startPixel.y);
      container.addChild(embeddedContainer);

      leftRenderer.render(director, clonedInstruction, embeddedContainer);
    }
  }

  private static calculateTooltipPosition(
    instr: TooltipRenderInstruction,
    padding: number,
    size: Point,
    director: PixiRenderingDirector
  ): Point {
    const minX: number = 0;
    const maxX: number = 76;

    let pos: Point;
    if (instr.startPixel.x > director.clientSize.x - maxX) {
      pos = instr.startPixel;
    } else {
      pos = instr.startPixel.add(new Point(0, -(padding * 2) - size.y));
    }

    if (pos.y < 0) {
      pos = instr.startPixel.add(new Point(0, instr.size.y + padding));
    }

    if (pos.x + size.x > director.clientSize.x - maxX) {
      pos.x = director.clientSize.x - size.x - maxX;
    }
    if (pos.y + size.y > director.clientSize.y) {
      pos.y = director.clientSize.y - size.y;
    }
    if (pos.x < minX) {
      pos.x = minX;
    }
    if (pos.y < 0) {
      pos.y = 0;
    }

    return pos;
  }
}
