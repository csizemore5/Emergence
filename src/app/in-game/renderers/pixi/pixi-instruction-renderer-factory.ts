/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { environment } from '../../../../environments/environment';
import { RenderInstruction } from '../../../libraries/rendering/render-instruction';
import { IRendererProvider } from './i-renderer-provider';
import { PixiBarRenderer } from './pixi-bar-renderer';
import { PixiCellRenderer } from './pixi-cell-renderer';
import { PixiHeaderRenderer } from './pixi-header-renderer';
import { PixiInstructionRenderer } from './pixi-instruction-renderer';
import { PixiLogRenderer } from './pixi-log-renderer';
import { PixiRectangleRenderer } from './pixi-rectangle-renderer';
import { PixiTextRenderer } from './pixi-text-renderer';
import { PixiTooltipRenderer } from './pixi-tooltip-renderer';

export class PixiInstructionRendererFactory implements IRendererProvider {
  private readonly handlers: (() => PixiInstructionRenderer)[];

  constructor() {
    this.handlers = [];

    if (!environment.isTestRun) {
      this.handlers['TooltipRenderInstruction'] = (): PixiInstructionRenderer => new PixiTooltipRenderer();
      this.handlers['CellRenderInstruction'] = (): PixiInstructionRenderer => new PixiCellRenderer();
      this.handlers['RectangleInstruction'] = (): PixiInstructionRenderer => new PixiRectangleRenderer();
      this.handlers['LogRenderInstruction'] = (): PixiInstructionRenderer => new PixiLogRenderer();
      this.handlers['ProgressBarRenderInstruction'] = (): PixiInstructionRenderer => new PixiBarRenderer();
      this.handlers['TextRenderInstruction'] = (): PixiInstructionRenderer => new PixiTextRenderer();
      this.handlers['HeaderInstruction'] = (): PixiInstructionRenderer => new PixiHeaderRenderer();
    }
  }

  public getRendererForInstruction(instr: RenderInstruction): PixiInstructionRenderer {
    if (this.handlers[instr.constructor.name]) {
      return this.handlers[instr.constructor.name]();
    }

    return null;
  }
}
