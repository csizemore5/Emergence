/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {RenderInstruction} from '../../../libraries/rendering/render-instruction';
import {PixiInstructionRenderer} from './pixi-instruction-renderer';

export interface IRendererProvider {
  getRendererForInstruction(instr: RenderInstruction): PixiInstructionRenderer;
}
