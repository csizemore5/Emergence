/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {AnimationBase} from './animation-base';
import {IAnimationTarget} from './i-animation-target';

export class FadeOutAnimation extends AnimationBase {
  private fadeRate: number;

  constructor(target: IAnimationTarget, fadeRate: number = 1) {
    super(target);

    this.fadeRate = fadeRate;
  }

  public update(delta: number): void {
    if (!this.target || delta <= 0) {
      return;
    }

    const decay: number = this.fadeRate * 0.01 * delta;
    this.target.alpha = Math.max(this.target.alpha - decay, 0);
  }
}
