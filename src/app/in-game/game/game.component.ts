/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { environment } from '../../../environments/environment';
import { Randomizer } from '../../libraries/randomizer';
import { ButtonInstruction } from '../../libraries/rendering/button-instruction';
import { CommandButtonInstruction } from '../../libraries/rendering/command-button-instruction';
import { RenderInstruction } from '../../libraries/rendering/render-instruction';
import { CharacterClass } from '../../libraries/setup/character-class';
import { PlayerActor } from '../../libraries/world/actors/player-actor';
import { GameLoopService } from '../../services/core-engine/game-loop.service';
import { GameSetupService } from '../../services/core-engine/game-setup.service';
import { WorldService } from '../../services/core-engine/world.service';
import { RenderInstructionGenerationService } from '../../services/rendering-service/render-instruction-generation.service';
import { DialogService } from '../../services/ui-service/dialog.service';
import { InputService } from '../../services/ui-service/input.service';

@Component({
  selector: 'em-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit, OnDestroy {
  public player: PlayerActor = null;

  public commandSlots: any[];
  public commandButtons: any[];

  private instrSub: Subscription;

  constructor(
    private inputService: InputService,
    private gameLoopService: GameLoopService,
    private setupService: GameSetupService,
    private dialogService: DialogService,
    private worldService: WorldService,
    private rigService: RenderInstructionGenerationService
  ) {
    this.player = worldService.player;
    this.commandSlots = [];
    this.commandButtons = [];
  }

  @HostListener('window:keydown', ['$event'])
  public keyEvent(event: KeyboardEvent): void {
    if (this.inputService.handleKeypress(event.keyCode, event.key, event.ctrlKey, event.shiftKey)) {
      event.preventDefault();
    }
  }

  public ngOnInit(): void {
    this.instrSub = this.rigService.instructionsGenerated.subscribe((instr: RenderInstruction[]) => this.onInstructionsGenerated(instr));

    // Start the game as needed
    if (environment.allowDebugMode && !this.gameLoopService.isInGame) {
      const characterClass: CharacterClass = Randomizer.getRandomEntry(this.setupService.availableClasses);
      this.gameLoopService.start(characterClass);
    }
  }

  public ngOnDestroy(): void {
    if (this.instrSub) {
      this.instrSub.unsubscribe();
    }
  }

  private onInstructionsGenerated(instr: RenderInstruction[]): void {
    // Clear out old instructions
    this.commandSlots.length = 0;
    this.commandButtons.length = 0;

    // Translate the command instructions
    const commandInstructions: CommandButtonInstruction[] = instr
      .filter((i: RenderInstruction): boolean => i instanceof CommandButtonInstruction)
      .map((i: RenderInstruction): CommandButtonInstruction => <CommandButtonInstruction>i);

    for (const ci of commandInstructions) {
      const item: any = {
        x: ci.startPixel.x,
        y: ci.startPixel.y - 2,
        hotkey: ci.hotkeyNumber,
        isEnabled: ci.isEnabled,
        isSelected: ci.alwaysRenderHovered,
        callback: ci.clickFunction,
        instruction: ci,
        slot: ci.slot
      };

      this.commandSlots.push(item);
    }

    // Translate the button instructions
    const buttonInstructions: ButtonInstruction[] = instr
      .filter((i: RenderInstruction): boolean => i instanceof ButtonInstruction && !(i instanceof CommandButtonInstruction))
      .map((i: RenderInstruction): ButtonInstruction => <ButtonInstruction>i);

    for (const b of buttonInstructions) {
      const button: any = {
        x: b.startPixel.x,
        y: b.startPixel.y,
        content: b.content,
        iconName: b.iconName,
        instruction: b
      };

      this.commandButtons.push(button);
    }
  }
}
