/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {AfterViewChecked, AfterViewInit, Component, ElementRef, NgZone, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {isUndefined} from 'util';
import {environment} from '../../../environments/environment';
import {IGenerationContext} from '../../libraries/rendering/generation/i-generation-context';
import {IRenderingDirector} from '../../libraries/rendering/i-rendering-director';
import {RenderInstruction} from '../../libraries/rendering/render-instruction';
import {Point} from '../../libraries/world/point';
import {WorldService} from '../../services/core-engine/world.service';
import {PixiTextureService} from '../../services/rendering-service/pixi-texture.service';
import {RenderInstructionGenerationService} from '../../services/rendering-service/render-instruction-generation.service';
import {RenderingService} from '../../services/rendering-service/rendering.service';
import {GoogleAnalyticsHelper} from '../../shared/utility/google-analytics-helper';
import {Logger} from '../../shared/utility/logger';
import {PixiInstructionRendererFactory} from '../renderers/pixi/pixi-instruction-renderer-factory';
import {PixiRenderingDirector} from '../renderers/pixi/pixi-rendering-director';

@Component({
  selector: 'em-renderer-container',
  templateUrl: './renderer-container.component.html',
  styleUrls: ['./renderer-container.component.scss']
})
export class RendererContainerComponent implements OnInit, OnDestroy, AfterViewInit, AfterViewChecked {
  @ViewChild('canvasContainer') public canvasContainer: ElementRef;
  @ViewChild('gameCanvas') public canvasRef: ElementRef;

  private canvas: HTMLCanvasElement;

  private width: number;
  private height: number;

  private defaultWidth: number = 42;
  private defaultHeight: number = 42;
  private charWidth: number = this.defaultWidth;
  private charHeight: number = this.defaultHeight;

  private maxColumns: number;
  private maxRows: number;

  private cellOffset: Point;

  private renderSub: Subscription;
  private generateSub: Subscription;
  private zone: NgZone;
  private renderer: IRenderingDirector = null;
  private lastZoomLevel: number;

  constructor(
    private renderingService: RenderingService,
    private pixiTextureService: PixiTextureService,
    private rigService: RenderInstructionGenerationService,
    private worldService: WorldService
  ) {
    this.cellOffset = new Point(0, 0);
    this.zone = new NgZone({ enableLongStackTrace: !environment.production });
  }

  public ngOnInit(): void {
    this.renderSub = this.renderingService.renderInvalidated.subscribe((isInitialRender: boolean): void => {
      this.autoSetDimensions(true, isInitialRender);
    });

    this.generateSub = this.rigService.instructionsGenerated.subscribe((instr: RenderInstruction[]): void =>
      this.zone.runOutsideAngular(() => this.renderer.render(instr))
    );
  }

  public ngOnDestroy(): void {
    if (this.renderSub) {
      this.renderSub.unsubscribe();
    }
    if (this.generateSub) {
      this.generateSub.unsubscribe();
    }
  }

  public ngAfterViewInit(): void {
    this.initializeCanvas();

    // For smaller resolutions, start us zoomed in a bit more
    if (this.canvasContainer.nativeElement.clientWidth <= 800) {
      this.renderingService.zoomLevel = 2;
    }

    this.autoSetDimensions(true, true);
  }

  public ngAfterViewChecked(): void {
    this.autoSetDimensions(false, false);
  }

  private initializeCanvas(): void {
    this.canvas = this.canvasRef.nativeElement;

    this.renderer = new PixiRenderingDirector(
      this.canvasRef.nativeElement,
      this.pixiTextureService,
      this.rigService,
      new PixiInstructionRendererFactory(),
      false
    );
  }

  private autoSetDimensions(alwaysRender: boolean, isInitialRender: boolean): void {
    const element: any = this.canvasContainer.nativeElement;
    const zoomLevel: number = this.renderingService.zoomLevel;
    const zoomChanged: boolean = this.lastZoomLevel !== zoomLevel;

    if (this.width !== element.clientWidth || this.height !== element.clientHeight || zoomChanged) {
      if (isUndefined(this.width)) {
        const resolution: string = `${element.clientWidth}x${element.clientHeight}`;
        Logger.info(`Canvas initial area: ${resolution}`);
        GoogleAnalyticsHelper.emitEvent('ClientInfo', 'InitialSize', resolution);
      }

      this.width = element.clientWidth;
      this.height = element.clientHeight;

      this.charWidth = this.defaultWidth * zoomLevel;
      this.charHeight = this.defaultHeight * zoomLevel;
      Logger.debug(`Setting character size to ${this.charWidth} x ${this.charHeight} on zoom level ${zoomLevel}`);

      this.maxColumns = Math.ceil(this.width / this.charWidth);
      this.maxRows = Math.ceil(this.height / this.charHeight);

      this.lastZoomLevel = zoomLevel;

      this.renderer.handleWidthInvalidated(this.width, this.height, zoomLevel);

      this.render(isInitialRender);
    } else if (alwaysRender) {
      this.render(isInitialRender);
    }
  }

  private render(isInitialRender: boolean): void {
    if (!this.renderer || !this.worldService.player) {
      return;
    }

    const center: Point = this.renderingService.getRenderCenteringCellPosition();
    this.cellOffset = new Point(center.x - Math.floor(this.maxColumns / 2) + 1, center.y - Math.floor(this.maxRows / 2) + 1);

    const context: IGenerationContext = {
      actor: this.worldService.player,
      isInitialRender,
      cellOffset: this.cellOffset,
      maxColumns: this.maxColumns,
      maxRows: this.maxRows,
      charSize: new Point(this.charWidth, this.charHeight),
      clientSize: new Point(this.width, this.height)
    };

    this.rigService.requestInstructions(context);
  }
}
