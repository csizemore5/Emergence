/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'em-pause-indicator',
  templateUrl: './pause-indicator.component.html',
  styleUrls: ['./pause-indicator.component.scss']
})
export class PauseIndicatorComponent implements OnInit {
  constructor() {}

  public ngOnInit(): void {}
}
