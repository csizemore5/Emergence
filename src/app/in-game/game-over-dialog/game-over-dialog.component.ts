/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Component, OnInit } from '@angular/core';
import { GameOverInfo } from '../../libraries/game-over-info';
import { GameLoopService } from '../../services/core-engine/game-loop.service';

@Component({
  selector: 'em-game-over-dialog',
  templateUrl: './game-over-dialog.component.html',
  styleUrls: ['./game-over-dialog.component.scss']
})
export class GameOverDialogComponent implements OnInit {
  public info: GameOverInfo;

  constructor(private gameLoopService: GameLoopService) {}

  public ngOnInit(): void {
    this.info = this.gameLoopService.gameOverInfo;
  }

  public restart(): void {
    this.gameLoopService.restart();
  }

}
