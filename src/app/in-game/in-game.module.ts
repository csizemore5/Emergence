/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {PixiTextureService} from '../services/rendering-service/pixi-texture.service';
import {RenderInstructionGenerationService} from '../services/rendering-service/render-instruction-generation.service';
import {RenderingService} from '../services/rendering-service/rendering.service';
import {SharedModule} from '../shared/shared.module';
import {GameOverDialogComponent} from './game-over-dialog/game-over-dialog.component';
import {GameComponent} from './game/game.component';
import {PauseIndicatorComponent} from './pause-indicator/pause-indicator.component';
import {RendererContainerComponent} from './renderer-container/renderer-container.component';

@NgModule({
  imports: [CommonModule, SharedModule],
  declarations: [GameOverDialogComponent, PauseIndicatorComponent, RendererContainerComponent, GameComponent],
  providers: [RenderInstructionGenerationService, RenderingService, PixiTextureService]
})
export class InGameModule {}
