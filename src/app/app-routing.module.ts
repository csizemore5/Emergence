/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes, UrlSerializer } from '@angular/router';
import { environment } from '../environments/environment';
import { CharacterManagementComponent } from './character-management/character-management/character-management.component';
import { ClassDetailsComponent } from './game-setup/class-details/class-details.component';
import { GameSetupComponent } from './game-setup/game-setup.component';
import { ActorHelpComponent } from './help/actor-help/actor-help.component';
import { ArticleNotFoundComponent } from './help/article-not-found/article-not-found.component';
import { CommandHelpComponent } from './help/command-help/command-help.component';
import { HelpArticleComponent } from './help/help-article/help-article.component';
import { HelpCategoryComponent } from './help/help-category/help-category.component';
import { HelpCreditsComponent } from './help/help-credits/help-credits.component';
import { HelpScreenComponent } from './help/help-screen/help-screen.component';
import { ReleaseDetailsComponent } from './help/release-details/release-details.component';
import { ActorDefinitionResolver } from './help/resolvers/actor-definition-resolver';
import { CommandResolver } from './help/resolvers/command-resolver';
import { HelpCategoryResolver } from './help/resolvers/help-category-resolver';
import { HelpTopicResolver } from './help/resolvers/help-topic-resolver';
import { ReleaseInfoResolver } from './help/resolvers/release-info-resolver';
import { GameOverDialogComponent } from './in-game/game-over-dialog/game-over-dialog.component';
import { GameComponent } from './in-game/game/game.component';
import { MainMenuComponent } from './menu/main-menu/main-menu.component';
import { CharacterClassResolver } from './shared/character-class-resolver';
import { GameActiveGuard } from './shared/guards/game-active.guard';
import { GameOverGuard } from './shared/guards/game-over.guard';
import { NotFoundComponent } from './shared/not-found/not-found.component';
import { LowerCaseUrlSerializer } from './shared/utility/lower-case-url-serializer';

const routes: Routes = [
  {
    path: 'index.html',
    redirectTo: '/menu'
  },
  {
    path: 'setup',
    component: GameSetupComponent,
    children: [
      {
        path: '',
        redirectTo: 'ACTOR_PLAYER_GAME',
        pathMatch: 'full'
      },
      {
        path: ':classId',
        component: ClassDetailsComponent,
        resolve: { classId: CharacterClassResolver }
      }
    ]
  },
  {
    path: 'help',
    component: HelpScreenComponent,
    children: [
      { path: '', redirectTo: '/help/general', pathMatch: 'full' },
      { path: 'general/credits', component: HelpCreditsComponent },
      { path: 'not-found', component: ArticleNotFoundComponent },
      { path: 'article-not-found', component: ArticleNotFoundComponent },
      { path: 'category-not-found', component: ArticleNotFoundComponent },
      {
        path: 'releases',
        resolve: { category: HelpCategoryResolver },
        children: [
          { path: '', component: HelpCategoryComponent, pathMatch: 'full' },
          { path: ':version', component: ReleaseDetailsComponent, resolve: { release: ReleaseInfoResolver } }
        ]
      },
      {
        path: 'commands',
        resolve: { category: HelpCategoryResolver },
        children: [
          { path: '', component: HelpCategoryComponent, pathMatch: 'full' },
          { path: ':command', component: CommandHelpComponent, resolve: { command: CommandResolver } }
        ]
      },
      {
        path: 'actors',
        resolve: { category: HelpCategoryResolver },
        children: [
          { path: '', component: HelpCategoryComponent, pathMatch: 'full' },
          { path: ':classId', component: ActorHelpComponent, resolve: { actor: ActorDefinitionResolver } }
        ]
      },
      {
        path: 'players',
        resolve: { category: HelpCategoryResolver },
        children: [
          { path: '', component: HelpCategoryComponent, pathMatch: 'full' },
          { path: ':classId', component: ClassDetailsComponent, resolve: { classId: CharacterClassResolver } }
        ]
      },
      {
        path: ':category',
        resolve: { category: HelpCategoryResolver },
        children: [
          { path: '', component: HelpCategoryComponent, pathMatch: 'full' },
          {
            path: ':topic',
            component: HelpArticleComponent,
            resolve: { topic: HelpTopicResolver }
          }
        ]
      }
    ]
  },
  {
    path: 'character',
    component: CharacterManagementComponent,
    outlet: 'dialog'
  },
  {
    path: 'play',
    component: GameComponent,
    canActivate: [GameActiveGuard]
  },
  {
    path: 'postgame',
    component: GameOverDialogComponent,
    canActivate: [GameOverGuard]
  },
  {
    path: 'menu',
    component: MainMenuComponent
  },
  {
    path: '',
    redirectTo: '/menu',
    pathMatch: 'full'
  },
  {
    path: 'emergence',
    redirectTo: '/menu',
    pathMatch: 'prefix'
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: environment.traceAngularRoutes })],
  exports: [RouterModule],
  providers: [GameActiveGuard, GameOverGuard, { provide: UrlSerializer, useClass: LowerCaseUrlSerializer }]
})
export class AppRoutingModule {}
